<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Connector;

use Psr\Log\LoggerInterface;
use GuzzleHttp\Psr7;
use Chill\BrusafeBundle\Util\UUIDUtil;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ITI41ProvideAndRegisterDocument
{
    /**
     *
     * @var LoggerInterface
     */
    protected $logger;
    
    /**
     *
     * @var UUIDUtil
     */
    protected $uuid_util;
    
    /**
     * Endpoint to send document
     *
     * @var string
     */
    protected $endpoint;
    
    /**
     * cert file
     *
     * @var string
     */
    protected $cert;
    
    /**
     * the soap boundary id, filled by `prepareContent`
     *
     * @var string
     */
    protected $soapBoundaryId;
    
    /**
     * the id of the source
     *
     * @var string
     */
    protected $source_id;
    
    /**
     * the source machine DNS name
     *
     * @var string
     */
    protected $source_machine;
    
    /**
     * The atna endpoint. Must be expressed as a full scheme (as
     * `tcps://api.qa.brusafe.be:6514`
     *
     * @var string
     */
    protected $atna_endpoint;
    
    const SOAP_ACTION = 'urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b';
    
    public function __construct(
        $cert,
        $endpoint,
        LoggerInterface $logger,
        UUIDUtil $uuid_util,
        $source_id,
        $source_machine,
        $atna_endpoint
        )
    {
        $this->cert      = $cert;
        $this->logger    = $logger;
        $this->uuid_util = $uuid_util;
        $this->endpoint  = $endpoint; // 'https://api.qa.brusafe.be:8003/services/repository'
        $this->source_id = $source_id;
        $this->source_machine = $source_machine;
        $this->atna_endpoint = $atna_endpoint;
    }
    
    /**
     * Make a SOAP request to the endpoint, to provide document
     * 
     * @param \DOMDocument $metadata The metadata of the documents
     * @param string $attachments the documents, formatted as MIME Boundary. Will be appended at the end of the request
     * @param \Chill\BrusafeBundle\Connector\TherapeuticRelation $relation The link between doctor and patient
     * @param string the uuid of the submission set, formatted as `urn:uuid:1234.etc`
     */
    public function provideAndRegisterDocument(
        \DOMDocument $metadata, 
        $attachments,
        TherapeuticRelation $relation,
        $submissionSetUuid
    ) {
        $message = $this->prepareContent(
            $this->addSamlInSoap(
                $this->prepareSoapXML($metadata, $relation)
                    ->saveXML(),
                $relation
                ), 
            $attachments
            );
        
        $this->sendRequest($message);
        
        $this->sendLog($relation, $submissionSetUuid);
    }
    
    protected function sendLog(
        TherapeuticRelation $relation,
        $submissionSetUuid
    ) {
        $message = $this->createLog($relation, $submissionSetUuid);
        $this->logger->debug("log message created", [ 'msg' => $message ]);
        
        $logger = new \Monolog\Logger('ihe');
        $handler = new \Monolog\Handler\SocketHandler($this->atna_endpoint);
        $handler->setPersistent(false);
        $formatter = new \Monolog\Formatter\LineFormatter("%message%");
        $handler->setFormatter($formatter);
        $logger->pushHandler($handler);
        
        $logger->info($message);
    }
    
    /**
     * 
     * 
     * @param \Chill\BrusafeBundle\Connector\TherapeuticRelation $relation
     * @param string $submissionSetUuid
     * @return string the xml of the log message
     */
    protected function createLog(
        TherapeuticRelation $relation,
        $submissionSetUuid
    ) {
        $logMessage = new LogMessage();
        
        $logMessage->addEventIdentification(
            "R", 
            new \DateTime(), 
            "110106", 
            "Export", 
            "ITI-41", 
            "Provide and Register Document Set-b");
        
        // source requestor
        $source = $logMessage->addActiveParticipant(
            // the pid of the process
            \getmypid(), 
            // the id of the source
            $this->source_id, 
            "110153", 
            "DCM", 
            "Source");
        $source->setAttribute("UserIsRequestor", "true");
        // 1 if dns is used in NetworkAccessPointID, 
        $source->setAttribute("NetworkAccessPointTypeCode", "1");
        // address to reach the source machine
        $source->setAttribute("NetworkAccessPointID", $this->source_machine);
        
        // human requestor
        // only if known
        $human = $logMessage->addActiveParticipant(
            // id of the user
            $relation->getAuthorId(),
            // name of the user who created the document
            $relation->getAuthorName(),
            // TODO: those information are hardcoded
            "112247003",
            "SNOMED_CT",
            "doctor");
        $human->setAttribute("UserIsRequestor", "true");
        
        // destination
        $dest = $logMessage->addActiveParticipant(
            // url of the endpoint
            $this->endpoint,
            // name of the endpoint
            "Brusafe+ network", 
            "110152", 
            "DCM", 
            "Destination");
        // 1 if dns is used in NetworkAccessPointID,
        $dest->setAttribute("NetworkAccessPointTypeCode", "1");
        // address to reach the dest machine
        $dest->setAttribute("NetworkAccessPointID", \parse_url($this->endpoint, PHP_URL_HOST));
        $dest->setAttribute("UserIsRequestor", "false");
        
        // audit source id
        $logMessage->addAuditSourceIdentification($this->source_id);
        
        // patient
        $code = $logMessage->createCodeElement('ParticipantObjectIDTypeCode', 
            "2", "RFC-3881", "Patient Number");
        $code->setAttribute("displayName", "Patient Number");
        $logMessage->addParticipantObjectIdentification(
            $relation->getPatientIdAsCX(), 
            "1", 
            "1",
            $code);
        
        // submission set
        $code = $logMessage->createCodeElement(
            'ParticipantObjectIDTypeCode',
            "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd",
            "IHE XDS Metadata",
            'submission set classificationNode'
            );
        $logMessage->addParticipantObjectIdentification(
            // uuid of the submission set
            $submissionSetUuid, 
            "2", 
            "20",
            $code
            );
        
        return $logMessage->getDocument()->saveXML();
    }
    
    /**
     * 
     * @param string $content
     * @return Psr7\Response
     */
    protected function sendRequest($content)
    {
        $client = new \GuzzleHttp\Client([
            'time_out' => '2.0',
            'base_uri' => $this->endpoint,
            'cert'     => $this->cert,
            'headers'  => array(
                'User-Agent' => 'GuzzleHttp/Chill',
                'Content-Type' => sprintf('multipart/related; '
                    . 'boundary=MIME_boundary; '
                    . 'type="application/soap+xml"; '
                    . 'start="<%s>"; ',
                   // . 'action="%s"',
                   // $this->soapBoundaryId,
                    $this->soapBoundaryId//,
                    //self::SOAP_ACTION
                    )
                )
        ]);
        
        try {
            
            $this->logger->debug("Sending request to brusafe+ network", [
                'content' => $content
            ]);
            
            $response = $client->request('POST', $this->endpoint, [
                'body' => $content
            ]);
            
            // log response
            $this->logger->debug("Response from brusafe+ network", [
                'content' => Psr7\str($response)
            ]);
            
            $this->detectErrors($response);
            
            return $response;
            
        } catch (\GuzzleHttp\Exception\RequestException $ex) {
            $this->logger->error("Error while sending document to server", [
                'response' => Psr7\str($ex->getResponse())
                ]);
            $this->logger->debug("Error while sending document to server: request", [
                'request' => Psr7\str($ex->getRequest())
                ]);
            
            throw $ex;
        }
    }
    
    /**
     * 
     * @param \GuzzleHttp\Psr7\Response $response
     * @return type
     * @throws \RuntimeException if the soap has error messages
     */
    protected function detectErrors(Psr7\Response $response)
    {
        $mime = new MimeBoundary($response->getBody());
        
        $string = \trim($mime->getPartContent(
            $mime->getStartContentId($response->getHeader('Content-Type'))
            ));
        
        $doc = new \DOMDocument();
        $doc->loadXML($string);
        $xpath = new \DOMXPath($doc);
        $xpath->registerNamespace('soap', "http://www.w3.org/2003/05/soap-envelope");
        $xpath->registerNamespace('ns3', "urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0");
        $errors = $xpath->query('/soap:Fault/text()|//ns3:RegistryError/@codeContext');
        
        
        if ($errors->length === 0) {
            return;
        } else {
            $message = "";
            for ($i=0; $i < $errors->length; $i++) {
                if ($errors->item($i) instanceof \DOMAttr) {
                    $message.= $errors->item($i)->value;
                } else {
                    $message .= $errors->item($i)."\n";
                }
            }
            
            throw new \RuntimeException($message);
        }
    }
    
    protected function prepareContent($soap, $attachments)
    {
        $boundaryid = $this->soapBoundaryId = $this->uuid_util->createUUID();
        
        $message = "\r\n";
		$message .= "--MIME_boundary\r\n";
		$message .= "Content-Type: application/soap+xml; charset=UTF-8\r\n";
		$message .= "Content-Transfer-Encoding: 8bit\r\n";
		$message .= "Content-ID: <".$boundaryid.">\r\n\r\n";
        $message .= $soap;
        $message .= $attachments;
        $message .= "--MIME_boundary--";
        
        return $message;
    }
    
    /**
     * add the saml string statement in soap request, just before the 
     * `</soap:Header>` end tag.
     * 
     * This function also check that the xml is still valid. For keeping the
     * request valid, the document cannot use the saml xml namespace than 
     * the namespace of the saml statement.
     * 
     * @param string $soap
     * @param \Chill\BrusafeBundle\Connector\TherapeuticRelation $relation
     * @return type
     * @throws \RuntimeException
     */
    protected function addSamlInSoap($soap, TherapeuticRelation $relation)
    {
        // append end of header with saml assertion
        $replaced = \preg_replace(
            '|<\/'.SoapBuilder::SOAP_PFX.':Header>|',
            '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'
            .$relation->getSamlString().
            '</wsse:Security>'
            // append end of saml header
            .'</'.SoapBuilder::SOAP_PFX.':Header>',
            $soap
            );
        
        // check that the xml is still valid
        libxml_use_internal_errors(true);

        $doc = simplexml_load_string($replaced);

        if ($doc === false) {
            $errors = libxml_get_errors();

            foreach ($errors as $error) {
                $this->logger->debug("The xml with the added saml assertion is "
                    . "not valid.", [ 
                        'errors' => sprintf("Error "
                            . "level: %d, "
                            . "code: %s, "
                            . "column: %s, "
                            . "message: %s", 
                            $error->level, 
                            $error->code, 
                            $error->column,
                            $error->message),
                        'xml' => $replaced
                    ]
                    );
            }
            
            throw new \RuntimeException("the saml assertion could not be "
                . "appended to the soap request");
        }
        
        return $replaced;
        
    }
    
    /**
     * 
     * @param \DOMDocument $metadata
     * @param TherapeuticRelation $relation
     */
    protected function prepareSoapXML(
        \DOMDocument $metadata, 
        TherapeuticRelation $relation
    ) {
        /// this part is freely inspirated by OLatignies, many thanks
		// Add WS Adressing header
		$objWSA = new SoapBuilder();
        
        $localMetadata = $objWSA->getDoc()->importNode($metadata->documentElement, true);
        $objWSA->getBody()->appendChild($localMetadata);
		$objWSA->addAction('urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b');
		$objWSA->addTo('https://api.qa.brusafe.be:8003/services/repository');
		$objWSA->addMessageID($this->uuid_util->createUUID(true));
		$objWSA->addReplyTo();

        return $objWSA->getDoc();
    }

}
