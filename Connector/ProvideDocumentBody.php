<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Connector;

use Chill\HealthBundle\Entity\Publication;
use Chill\BrusafeBundle\Util\UUIDUtil;

/**
 * Create the XDS metadata required by the XDS "Provide And Register Document"
 * transaction (IHE ITI-41).
 * 
 * The submission set should concern only one patient, with the same therapeutic
 * relation.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ProvideDocumentBody
{
    /**
     *
     * @var UUIDUtil
     */
    protected $uuidUtil;
    
    /**
     * XDSSubmissionSet.sourceId
     * 
     * OID identifying the instance of the Document Source that contributed 
     * the Submission Set. 
     *
     * @var string
     */
    protected $sourceId;
    
    /**
     *
     * @var TherapeuticRelation
     */
    protected $therapeuticRelation;
    
    /**
     * Publications 
     *
     * @var Publication[]
     */
    protected $publications;
    
    /**
     * Language code of the documents
     *
     * @var string
     */
    protected $languageCode = 'fr_FR';
    
    /**
     * The title of the submission set
     *
     * @var string
     */
    protected $title = 'XDS Submission set';
    
    /**
     *
     * @var \DOMDocument
     */
    private $dom;
    
    /**
     * The namespace for xds.b namespace
     */
    const NS_XDS = 'urn:ihe:iti:xds-b:2007';
    
    /**
     * The namespace for lcm
     */
    const NS_LCM = 'urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0';
    
    /**
     * The namespace for RIM
     */
    const NS_RIM = 'urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0';
    
    /**
     * The namespace for XOP reference
     */
    const NS_XOP = "http://www.w3.org/2004/08/xop/include";
    
    /**
     * The namespace for namespaces
     */
    const NS_XMLNS = 'http://www.w3.org/2000/xmlns/';
    
    public function __construct(
        UUIDUtil $uuid_util,
        $source_id
    ) {
      $this->uuidUtil = $uuid_util;
      $this->sourceId = $source_id; // '2.25.275065460301684640686606660420208408884'
    }
    
    /**
     * Set the therapeutic link for the subsmission set.
     * 
     * @param \Chill\BrusafeBundle\Connector\TherapeuticRelation $therapeuticRelation
     */
    public function setTherapeuticLink(TherapeuticRelation $therapeuticRelation)
    {
        $this->therapeuticRelation = $therapeuticRelation;
    }
    
    /**
     * Add a document to the submission set
     * 
     * @param Publication $publication
     */
    public function addDocument(
        Publication $publication
    ) {
        $this->publications[] = $publication;
    }
    
    public function createPublish()
    {
        $dom = $this->dom = new \DOMDocument('1.0');
        
        // create root element (ProvideAndRegisterDocumentSetRequest
        $root = $dom->createElementNS(self::NS_XDS, 
            'xdsb:ProvideAndRegisterDocumentSetRequest');
        $root->setAttributeNS(self::NS_XMLNS, 'xmlns', self::NS_RIM);
        $root->setAttributeNS(self::NS_XMLNS, 'xmlns:lcm', self::NS_LCM);
        $root->setAttributeNS(self::NS_XMLNS, 'xmlns:xdsb', self::NS_XDS);
        $root->setAttributeNS(self::NS_XMLNS, 'xmlns:xop', self::NS_XOP);
        
        
        $submit = $dom->createElementNS(self::NS_LCM, 'SubmitObjectsRequest');
        $root->appendChild($submit);
        
        $registryObjectList = $dom->createElementNS(self::NS_RIM, 'RegistryObjectList');
        $submit->appendChild($registryObjectList);
        
        $submissionSetUuid = $this->addRegistryPackage($registryObjectList);
        
        foreach ($this->publications as $publication) {
            $this->createExtrinsicObject($registryObjectList, $publication);
        }
        $dom->appendChild($root);
        
        // append each document
        $attachments = "";
        foreach ($this->publications as $publication) {
            $document = $dom->createElementNS(self::NS_XDS, 'xdsb:Document');
            $document->setAttribute(
                "id", 
                $this->toUrnUUID($publication->getId())
                );
            $include = $dom->createElementNS(self::NS_XOP, 'xop:Include');
            $include->setAttribute("href", 
                $this->createDocumentContentID($publication));
            $root->appendChild($document);
            $document->appendChild($include);
            
            $attachments .= $this->createBoundaryPart($publication);
        }
        
        return [ $dom, $attachments, $submissionSetUuid ];
    }
    
    /**
     * Create the MIME part with boundary.
     * 
     * @author OLatignies <https://github.com/olatignies>
     * @param Publication $publication
     * @param boolean $last
     * @return string
     */
    protected function createBoundaryPart(Publication $publication)
    {
        $message = "--MIME_boundary\r\n";
		$message .= "Content-Type: text/xml; charset=UTF-8\r\n";
		$message .= "Content-Transfer-Encoding: 8bit\r\n";
		$message .= "Content-ID: <".$this->createDocumentContentID($publication).">\r\n\r\n";
		$message .= $publication->getData();
		$message .= "\r\n";
        
		$message .= "\r\n";
        
        return $message;
    }
    
    /**
     * Return a string with the content-id of the document.
     * 
     * This content-id will be used : 
     * - in MIME_boundary
     * - in `<xop:Include href="">`, href attribute
     * 
     * @param Publication $publication
     * @return string
     */
    protected function createDocumentContentID(Publication $publication)
    {
        return $publication->getId()."@chill.social.cda.xml";
    }
    
    /**
     * add a registry package, which package all the document, folder, etc.
     * for submission
     * 
     * @param \DOMElement $registryObjectList
     * @return string the uuid of the submission set
     */
    protected function addRegistryPackage(\DOMElement $registryObjectList)
    {
        $registryId = $this->createUUID();
        
        $registryPackage = $this->dom->createElementNS(self::NS_RIM, 
            'RegistryPackage');
        $registryPackage->setAttributeNS(self::NS_RIM, 'objectType', 
            'urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage');
        $registryPackage->setAttributeNS(self::NS_RIM, 'id', $registryId);
        // add submission time
        $registryPackage->appendChild($this->createSlot('submissionTime', 
            (new \DateTime())->format('YmdHis')));
        // add title
        $registryPackage->appendChild(
            $this->createLocalizedString($this->title)
            );
        
        
        
        $this->createClassCode($registryPackage, $registryId);
        $this->createClassificationAsSubmissionSet($registryPackage, $registryId);
        $this->createAuthor($registryPackage, $registryId, 'submission_set');
        $this->createSubmissionSetSourceId($registryPackage, $registryId);
        $submissionSetUniqueId = $this->createSubmissionSetUniqueId($registryPackage, $registryId);
        $this->createSubmmissionSetPatientId($registryPackage, $registryId);
        
        $registryObjectList->appendChild($registryPackage);
        
        return $submissionSetUniqueId;
    }
    
    /**
     * Create a `Classification` Element which indicate that the document 
     * is a submission set.
     * 
     * @param \DOMElement $registryPackage
     * @param string $registryId
     */
    public function createClassificationAsSubmissionSet(\DOMElement $registryPackage, $registryId)
    {
        /*
         * <rim:Classification classificationNode="urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd"
                                      classifiedObject="urn:uuid:701000d8-b22e-4015-b0ca-609e30cb1893 voir RegistryPackage"
                                      id="urn:uuid:9f25c0a2-fb2e-4715-997b-a2f7c8e2e6e6"
                                      objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification"/>
         */
        
        $class = $this->dom->createElementNS(self::NS_RIM, 'Classification');
        $class->setAttributeNS(self::NS_RIM, 'classificationNode', 
            "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd");
        $class->setAttributeNS(self::NS_RIM, 'classifiedObject', $registryId);
        $class->setAttributeNS(self::NS_RIM, 'id', $this->createUUID());
        $class->setAttributeNS(self::NS_RIM, 'objectType', "urn:oasis:names:"
            . "tc:ebxml-regrep:ObjectType:RegistryObject:Classification");
        
        $registryPackage->appendChild($class);
    }
    
    /**
     * Create the classCode metadata, which indicate the class of the document.
     * 
     * Abrumet code are :
     * 
     * ```
     * <Concept code="11221000146107" codeSystem="2.16.840.1.113883.6.96" codeSystemName="Snomed CT" displayName="Note"/>
     * <Concept code="371534008" codeSystem="2.16.840.1.113883.6.96" codeSystemName="Snomed CT" displayName="Summary Report"/>
     * <Concept code="9531000146107" codeSystem="2.16.840.1.113883.6.96" codeSystemName="Snomed CT" displayName="Letter"/>
     * ```
     * 
     * @param \DOMElement $registryPackage
     * @param string $registryId
     */
    private function createClassCode(\DOMElement $registryPackage, $registryId)
    {
        $classification = $this->createClassification(
            'urn:uuid:aa543740-bdda-424e-8c96-df4873be8500', 
            $registryId, 
            '371534008' // value for "Summary Report"
            );
        $classification->appendChild($this->createSlot('codingScheme', 
            '2.16.840.1.113883.6.96'));
        $classification->appendChild($this
            ->createLocalizedString("Summary Report"));
        
        $registryPackage->appendChild($classification);
    }
    
    private function createSubmissionSetSourceId(\DOMElement $registryPackage, $registryId)
    {
        $extId = $this->createExternalIdentifier(
            "urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832", 
            $registryId, 
            $this->sourceId, 
            "XDSSubmissionSet.sourceId"
            );
        
        $registryPackage->appendChild($extId);
    }
    
    /**
     * Create the "XDSSubmissionSet.uniqueId".
     * 
     * In this implementation, the submission set is randomly chosen.
     * 
     * return the uuid of the submission set
     * 
     * @param \DOMElement $registryPackage
     * @param type $registryId
     * @return string
     */
    private function createSubmissionSetUniqueId(\DOMElement $registryPackage, $registryId)
    {
        $uuid = $this->uuidUtil->UUIDtoOID($this->createUUID());
        
        $extId = $this->createExternalIdentifier(
            "urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8", 
            $registryId,
            $uuid, 
            "XDSSubmissionSet.uniqueId");
        
        $registryPackage->appendChild($extId);
        
        return $uuid;
    }
    
    /**
     * Create the "XDSSubmissionSet.patientId"
     * 
     * The patient id is extracted from therapeuticLink
     * 
     * @param \DOMElement $registryPackage
     * @param string $registryId
     */
    private function createSubmmissionSetPatientId(\DOMElement $registryPackage, $registryId)
    {
        $extId = $this->createExternalIdentifier(
            "urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446", 
            $registryId,
            $this->therapeuticRelation->getPatientIdAsCX(),
            "XDSSubmissionSet.patientId");
        
        $registryPackage->appendChild($extId);
    }
    
    /**
     * 
     * @param \DOMElement $registryPackage
     * @param string $registryId
     * @param string $place `submission_set` or `document`
     */
    private function createAuthor(\DOMElement $registryPackage, $classifiedObject, $place)
    {
        $registryPackage->appendChild(new \DOMComment("The author of the document"));
        /*
         <rim:Classification
                classificationScheme="urn:uuid:a7058bb9-b4e4-4307-ba5b-
                e3f0ab85e12d"
                classifiedObject="theDocument"
                nodeRepresentation="">
                <!-- nodeRepresentation intentionally left blank -->
                <rim:Slot name="authorPerson">
                <!-- shall be single valued -->
                <rim:ValueList>
                <rim:Value>name of author</rim:Value>
                </rim:ValueList>
                </rim:Slot>
                <rim:Slot name="authorInstitution">
                <!-- may be multivalued -->
                <rim:ValueList>
                <rim:Value> Some
                Hospital^^^^^^^^^1.2.3.4.5.6.7.8.9.1789.45</rim:Value>
                </rim:ValueList>
                </rim:Slot>
                <rim:Slot name="authorRole">
                <!-- may be multivalued -->
                <rim:ValueList>
                <rim:Value>name of role</rim:Value>
                </rim:ValueList>
                </rim:Slot>
                <rim:Slot name="authorSpecialty">
                <!-- may be multivalued -->
                <rim:ValueList>
                <rim:Value>specialty of
                author</rim:Value>
                </rim:ValueList>
                </rim:Slot>
        </rim:Classification>
         */
        
        switch($place) {
            case 'submission_set' :
                $classificationScheme = "urn:uuid:a7058bb9-b4e4-4307-ba5b-e3f0ab85e12d";
                break;
            case 'document':
                $classificationScheme = "urn:uuid:93606bcf-9494-43ec-9b4e-a7748d1a838d";
                break;
            default:
                throw new \RuntimeException("the value of place should be 'submi"
                    . "ssion_set' or 'document', '$place' given.");
        }
        
        $class = $this->createClassification(
            $classificationScheme, 
            $classifiedObject, 
            "" // intentionnaly left blank
            );
        $class->appendChild($this->createSlot("authorPerson", 
            $this->therapeuticRelation->getAuthorName()));
        $class->appendChild($this->createSlot("authorRole", 
            $this->therapeuticRelation->getAuthorRole()));
        
        $registryPackage->appendChild($class);
    }
    
    /**
     * Create the `ExtrinsicObject` element, which describe the metadata
     * for a document.
     * 
     * @param \DOMElement $registryObjectList
     * @param Publication $publication
     */
    protected function createExtrinsicObject(
        \DOMElement $registryObjectList, 
        Publication $publication
    ) {
        $exObject = $this->dom->createElementNS(self::NS_RIM, 'ExtrinsicObject');
        $exObject->setAttributeNS(self::NS_RIM, 'id', 
            $this->toUrnUUID($publication->getId()));
        $exObject->setAttributeNS(self::NS_RIM, 'mimeType', 'text/xml');
        $exObject->setAttributeNS(self::NS_RIM, 'objectType', "urn:uuid:"
            . "7edca82f-054d-47f2-a032-9b2a5b5186c1");
        // only approved documents can be sent
        $exObject->setAttributeNS(self::NS_RIM, 'status', 
            "urn:oasis:names:tc:ebxml-regrep:StatusType:Approved");
        
        $registryObjectList->appendChild($exObject);
        
        $exObject->appendChild($this->createSlot('languageCode', 
            $this->languageCode));
        $exObject->appendChild($this->createSlot('creationTime',
            $publication->getDate()->format('YmdHis')));
        $exObject->appendChild($this->createSlot(
            'sourcePatientId',
            \htmlspecialchars(
                $this->therapeuticRelation->getPatientIdAsCX(), 
                ENT_XML1
                )
            ));
        
        $this->createClassCodeDocument($exObject, $publication);
        $this->createConfidentialityCode($exObject, $publication);
        $this->createFormatCode($exObject, $publication);
        $this->createHealthCareFacilityCode($exObject, $publication);
        $this->createPracticeSettingCode($exObject, $publication);
        $this->createDocumentTypeCode($exObject, $publication);
        $this->createAuthor(
            $exObject, 
            $this->toUrnUUID($publication->getId()), 
            'document'
            );
        
        $exObject->appendChild($this->createExternalIdentifier(
            "urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab", 
            $this->toUrnUUID($publication->getId()), 
            $publication->getId(), 
            "XDSDocumentEntry.uniqueId"
            ));
        $exObject->appendChild($this->createExternalIdentifier(
            "urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427", 
            $this->toUrnUUID($publication->getId()), 
            $this->therapeuticRelation->getPatientIdAsCX(), 
            "XDSDocumentEntry.patientId"
            ));
    }
    
    /**
     * Create the class code for the document.
     * 
     * Currently, the code is hardcoded to a "summary report"
     * 
     * @param \DOMElement $extrinsicObject
     */
    private function createClassCodeDocument(
        \DOMElement $extrinsicObject,
        Publication $publication
    ) {
        /*
            <rim:Classification classificationScheme="urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a"
                                  classifiedObject="urn:uuid:8960dfa1-b685-4747-96dc-d637dc553460===document"
                                  id="urn:uuid:aa976c47-d766-4777-b8f4-acd6a5498245"
                                  nodeRepresentation="371534008"
                                  objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                <rim:Slot name="codingScheme">
                    <rim:ValueList>
                        <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                    </rim:ValueList>
                </rim:Slot>
                <rim:Name>
                    <rim:LocalizedString value="Summary Report"/>
                </rim:Name>
              </rim:Classification>
         */
        
        $extrinsicObject->appendChild(new \DOMComment("The classCode of the "
            . "document. Currently hardcoded"));
        $class = $this->createClassification(
            "urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a", 
            $this->toUrnUUID($publication->getId()), 
            '371534008'
            );
        $class->appendChild($this->createSlot("codingScheme", "2.16.840.1.113883.6.96"));
        $class->appendChild($this->createLocalizedString("Summary Report"));
        
        $extrinsicObject->appendChild($class);
        
    }
    
    protected function createConfidentialityCode(
        \DOMElement $extrinsicObject,
        Publication $publication
    ) {
        $extrinsicObject->appendChild(new \DOMComment("Confidentiality code. "
            . "Hardcoded as 'normal'."));
        /*
            <rim:Classification classificationScheme="urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f"
                        classifiedObject="urn:uuid:8960dfa1-b685-4747-96dc-d637dc553460===document"
                        id="urn:uuid:8796b341-d525-4f04-85ad-5edf73f8dd77"
                        nodeRepresentation="N"
                        objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                <rim:Slot name="codingScheme">
                    <rim:ValueList>
                        <rim:Value>"1.3.6.1.4.1.12559.11.26.5.1.3"</rim:Value>
                    </rim:ValueList>
                </rim:Slot>
                <rim:Name>
                    <rim:LocalizedString value="Normal"/>
                </rim:Name>
            </rim:Classification>
         */
        
        $class = $this->createClassification(
            "urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f", 
            $this->toUrnUUID($publication->getId()), 
            "N");
        $class->appendChild($this->createSlot("codingScheme", 
            "2.16.840.1.113883.5.25"));
        $class->appendChild($this->createLocalizedString("Normal"));
        
        $extrinsicObject->appendChild($class);
    }
    
    /**
     * Create the format code.
     * 
     * Hardcoded to "Exchange of Personal Health Records"
     * 
     * @param \DOMElement $extrinsicObject
     * @param Publication $publication
     */
    protected function createFormatCode(
        \DOMElement $extrinsicObject,
        Publication $publication
    ) {
        $extrinsicObject->appendChild(new \DOMComment("Format code. Hardcoded on"
            . " \"exchange of personal record\""));
        
        /*
        <rim:Classification classificationScheme="urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d"
                                      classifiedObject="urn:uuid:8960dfa1-b685-4747-96dc-d637dc553460===document"
                                      id="urn:uuid:6fcea343-403a-4a15-a167-c5ce3c7f19e6"
                                      nodeRepresentation="urn:ihe:pcc:xphr:2007"
                                      objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
            <rim:Slot name="codingScheme">
                <rim:ValueList>
                    <rim:Value>1.3.6.1.4.1.19376.1.2.3</rim:Value>
                </rim:ValueList>
            </rim:Slot>
            <rim:Name>
                <rim:LocalizedString value="Exchange of Personal Health Records"/>
            </rim:Name>
        </rim:Classification>
         */
        
        $class = $this->createClassification(
            "urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d", 
            $this->toUrnUUID($publication->getId()), 
            "urn:ihe:pcc:xphr:2007"
            );
        $class->appendChild($this->createSlot("codingScheme", 
            "1.3.6.1.4.1.19376.1.2.3"));
        $class->appendChild($this
            ->createLocalizedString("Exchange of Personal Health Records"));
        
        $extrinsicObject->appendChild($class);
    }
    
    /**
     * Create the health care facility code.
     * 
     * Currently hardcoded to "Outpatient"
     * 
     * @param \DOMElement $extrinsicObject
     * @param Publication $publication
     */
    protected function createHealthCareFacilityCode(
        \DOMElement $extrinsicObject,
        Publication $publication
    ) {
        $extrinsicObject->appendChild(new \DOMComment("Heathcare facility code. "
            . "Hardcoded to \"Huisartspraktijk (zelfstandig of groepspraktijk)\" "
            . "Stuck to old nictiz code."));
        
        /*
        <rim:Classification classificationScheme="urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1"
                            classifiedObject="urn:uuid:8960dfa1-b685-4747-96dc-d637dc553460===document"
                            id="urn:uuid:94598bcd-136f-4b12-834c-3d89969a739d"
                            nodeRepresentation="Outpatient"
                            objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
            <rim:Slot name="codingScheme">
                <rim:ValueList>
                    <rim:Value>1.3.6.1.4.1.12559.11.26.5.1.7</rim:Value>
                </rim:ValueList>
            </rim:Slot>
            <rim:Name>
                <rim:LocalizedString value="Outpatient"/>
            </rim:Name>
        </rim:Classification>
         */
        
        $class = $this->createClassification(
            "urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1", 
            $this->toUrnUUID($publication->getId()), 
            // "Outpatient"  FOR ABRUMET
            "Z3" // for NICTIZ
            );
        $class->appendChild($this->createSlot("codingScheme", 
            // "2.16.840.1.113883.5.11" FOR ABRUMET
            "2.16.840.1.113883.2.4.15.1060" // for nictiz
            ));
        $class->appendChild($this->createLocalizedString(
            // "Outpatient" for abrumet
            "Huisartspraktijk (zelfstandig of groepspraktijk)"
            ));
        
        $extrinsicObject->appendChild($class);
    }
    
    /**
     * Create the "practice setting" code. 
     * 
     * Currently hardcoded to "general physician"
     * 
     * @param \DOMElement $extrinsicObject
     * @param Publication $publication
     */
    protected function createPracticeSettingCode(
        \DOMElement $extrinsicObject,
        Publication $publication
    ) {
        $extrinsicObject->appendChild(new \DOMComment("Practice setting code. "
            . "Hardcoded to 'General physician'"));
        
        /*
        <Classification 
            classificationScheme="urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead" 
            classifiedObject="urn:uuid:205d4042-6058-655e-e37e-79f33fe361a0" 
            nodeRepresentation="59058001" 
            id="urn:uuid:883d38ce-1a54-41a1-8eb6-869a6bc0bfc6">
            <Slot name="codingScheme">
                <ValueList>
                    <Value>2.16.840.1.113883.6.96</Value>
                </ValueList>
            </Slot>
            <Name>
                <LocalizedString  value="General physician"/>
            </Name>
        </Classification>
         */
        
        $class = $this->createClassification(
            "urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead", 
            $this->toUrnUUID($publication->getId()), 
            // "59058001" FOR ABRUMEt
            "45899008"
            );
        $class->appendChild($this->createSlot("codingScheme", 
            // "2.16.840.1.113883.6.96" FOR ABRUMET
            "2.16.840.1.113883.6.96" // for NICTIZ
            ));
        $class->appendChild($this->createLocalizedString(
            // "General physician" FOR ABRUMET
            "Free-standing laboratory facility" // for nictiz
            ));
        
        $extrinsicObject->appendChild($class);
    }
    
    /**
     * Create the document type code.
     * 
     * Currently hardcoded to 
     * 
     * ```
     * <Concept code="DEMO-Consult" 
     *  codeSystem="1.3.6.1.4.1.12559.11.4.9" 
     *  codeSystemName="1.3.6.1.4.1.21367.100.1" 
     *  displayName="Consultation"/>
     * ```
     * 
     * @param \DOMElement $extrinsicObject
     * @param Publication $publication
     */
    protected function createDocumentTypeCode(
        \DOMElement $extrinsicObject,
        Publication $publication
    ) {
        $extrinsicObject->appendChild(new \DOMComment("Document type code. "
            //. "Linked to Abrument 'content type' code. "
            . "Hardcoded to "
            . "'medisch consultverslag' "));
        /*
        <rim:Classification classificationScheme="urn:uuid:f0306f51-975f-434e-a61c-c59651d33983"
                                      classifiedObject="urn:uuid:8960dfa1-b685-4747-96dc-d637dc553460===document"
                                      id="urn:uuid:11435c29-8eba-43e7-a3c9-20b7c82a1f52"
                                      nodeRepresentation="371531000"
                                      objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
                    <rim:Slot name="codingScheme">
                        <rim:ValueList>
                            <rim:Value>2.16.840.1.113883.6.96</rim:Value>
                        </rim:ValueList>
                    </rim:Slot>
                    <rim:Name>
                        <rim:LocalizedString value="medisch consultverslag"/>
                    </rim:Name>
                  </rim:Classification>
         */
        
        $class = $this->createClassification(
            "urn:uuid:f0306f51-975f-434e-a61c-c59651d33983", 
            $this->toUrnUUID($publication->getId()), 
            // "DEMO-Consult" FOR ABRUMET
            "371531000" // for nictiz
            );
        $class->appendChild($this->createSlot("codingScheme", 
            // "1.3.6.1.4.1.21367.100.1" FOR ABRUMT
            "2.16.840.1.113883.6.96" // FOR NICTIZ
            ));
        $class->appendChild($this->createLocalizedString(
            // "Consultation" FOR ABRUMET
            "medisch consultverslag" // FOR NICTIZ
            ));
        
        $extrinsicObject->appendChild($class);
        
    }
    
    /**
     * Create a classification element, which contains : 
     * 
     * - a classificationScheme
     * - a classifiedObject
     * - an id
     * - a nodeRepresentation (a value)
     * 
     * Note that some `Classification` element does not contains all those
     * attributes: this function cannot be use in this case.
     * 
     * @param string $classificationScheme
     * @param string $classifiedObject
     * @param string $nodeRepresentation
     * @return \DOMElement
     */
    protected function createClassification(
        $classificationScheme,
        $classifiedObject,
        $nodeRepresentation
    ) {
        $classification = 
            $this->dom->createElementNS(self::NS_RIM, 'Classification');
        $classification
            ->setAttributeNS(self::NS_RIM, 'classificationScheme', 
                $classificationScheme);
        $classification
            ->setAttributeNS(self::NS_RIM, 'classifiedObject', 
                $classifiedObject);
        $classification
            ->setAttributeNS(self::NS_RIM, 'id', $this->createUUID());
        $classification
            ->setAttributeNS(self::NS_RIM, 'nodeRepresentation', 
                $nodeRepresentation);
        $classification
            ->setAttributeNS(self::NS_RIM, 'objectType', "urn:oasis:names:tc:"
                . "ebxml-regrep:ObjectType:RegistryObject:Classification");
        
        return $classification;
    }
    
    /**
     * 
     * @param string $identificationScheme The identification scheme of the external identifier
     * @param string $registryObject a reference to the object described, i.e. the RegistryObject's id
     * @param string $value the value of the identifier
     * @param string $localizedString the display name of the identifier
     * @return \DOMElement
     */
    protected function createExternalIdentifier(
        $identificationScheme,
        $registryObject,
        $value,
        $localizedString
    ) {
        $extId = 
            $this->dom->createElementNS(self::NS_RIM, 'ExternalIdentifier');
        $extId
            ->setAttributeNS(self::NS_RIM, 'identificationScheme', 
                $identificationScheme);
        $extId
            ->setAttributeNS(self::NS_RIM, 'registryObject', 
                $registryObject);
        $extId
            ->setAttributeNS(self::NS_RIM, 'id', $this->createUUID());
        $extId
            ->setAttributeNS(self::NS_RIM, 'value', $value);
        $extId
            ->setAttributeNS(self::NS_RIM, 'objectType', "urn:oasis:names:"
                . "tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier");
        $extId->appendChild($this->createLocalizedString($localizedString));
        
        return $extId;
    }
    
    /**
     * Create a slot element with a valueList embedded
     * 
     * @param string $name
     * @param string $value
     * @return \DOMElement
     */
    protected function createSlot($name, $value)
    {
        $slot = $this->dom->createElementNS(self::NS_RIM, 'Slot');
        $slot->setAttributeNS(self::NS_RIM, 'name', $name);
        
        $valueList = $this->dom->createElementNS(self::NS_RIM, 'ValueList');
        $slot->appendChild($valueList);
        
        $value = $this->dom->createElementNS(self::NS_RIM, 'Value', $value);
        $valueList->appendChild($value);
        
        return $slot;
    }
    
    /**
     * 
     * @param string $string the content
     * @return \DOMElement
     */
    protected function createLocalizedString($string)
    {
        $name = $this->dom->createElementNS(self::NS_RIM, 'Name');
        $localizedString = $this->dom->createElementNS(self::NS_RIM, 
            'LocalizedString');
        $name->appendChild($localizedString);
        $localizedString->setAttributeNS(self::NS_RIM, 'value', $string);
        
        
        return $name;
            
    }
    
    /**
     * Return an uuid on the format `urn:uuid:$uuid`
     * 
     * @param string $uuid
     * @return string
     */
    protected function toUrnUUID($uuid)
    {
        return $this->uuidUtil->toUrnUUID($uuid);
    }
    
    /**
     * Create an uuid, on the format `urn:uuid:$uuid`
     * 
     * @return string
     */
    protected function createUUID()
    {
        return $this->uuidUtil->createUUID(true);
    }
    
    protected function UUIDtoOID($uuid)
    {
        return \base_convert(\str_replace("-", "", $uuid), 16, 10);
    }
    
}
