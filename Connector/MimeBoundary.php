<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Connector;

use Psr\Http\Message\StreamInterface;

/**
 * Class which read MIME boundary responses
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MimeBoundary
{
    /**
     * Contains the parts of the boundary.
     * 
     * Keys are `Content-ID` of the array
     *
     * @var array
     */
    private $parts = array();
    
    public function __construct($content) 
    {
        $this->read($content);
    }
    
    private function read($content) 
    {
        if ($content instanceof StreamInterface) {
            // reset stream
            $content->rewind();
            $body = $content->getContents();
        } else {
            $body = $content;
        }
        
        // read line by line
        $separator = "\r\n";
        $line = strtok($body, $separator);

        while ($line !== false) {
            
            if (substr($line, 0, 2) === "--") {
                // test if we are at the beggining or end of MIME
                if (substr($line, -2) === "--") {
                    // this is the end of the MIME
                    // register the exiting part to the main parts
                    $this->parts[$id] = $part;

                } else {
                    // this is the beginning of a MIME
                    $part = $this->createEmptyPart();
                    // we get the header
                }
            // a part has already begun. We parse it
            } elseif (isset($part)) {
                if (substr($line, 0, 13) === "Content-Type:") {
                    $part['content_type'] = $this->getContentType($line);
                } elseif (substr($line, 0, 26) === "Content-Transfer-Encoding:" ) {
                    $part['content_transfer_encoding'] = $this->getContentTransferEncoding($line);
                } elseif (substr($line, 0, 11) === "Content-ID:") {
                    $id = $this->getContentId($line);
                } else {
                    // we parse the content here
                    $part['content'] .= $line;
                }
            } else {
                // we are not in a part. continue to next line
            }
            
            // go to next line
            $line = strtok($separator);
        }
    }
    
    public function getIds()
    {
        return array_keys($this->parts);
    }
    
    public function hasPart($id)
    {
        return array_key_exists($id, $this->parts);
    }
    
    public function getPartContent($id)
    {
        return $this->parts[$id]['content'];
    }
    
    /**
     * Extract the "start content id" from the 
     * headers.
     * 
     * @param string[] $contentTypeHeader the content-type (the function `GuzzleHttp\Psr7\Response:getHeader('Content-Type')` will return the headers inside an array
     * @return string
     * 
     */
    public static function getStartContentId(array $contentTypeHeader)
    {
        $explode = \explode(";", $contentTypeHeader[0]);
        
        foreach ($explode as $part) {
            $part = \trim($part);
            if (substr($part, 0, 6) === "start=") {
                return substr($part, 7, \strlen($part)-8);
            }
        }
    }
    
    /**
     * Isolate Content-Type
     * 
     * @param string $line
     * @return string
     */
    private function getContentType($line)
    {
        $parts = \explode(":", $line);
        
        return \trim($parts[1]);
    }
    
    /**
     * Isolate Content-Transfer-Encoding
     * 
     * @param string $line
     * @return string
     */
    private function getContentTransferEncoding($line)
    {
        $parts = \explode(":", $line);
        
        return \trim($parts[1]);
    }
    
    private function getContentId($line)
    {
        $parts = \explode(":", $line);
        
        return \trim($parts[1]);
    }
    
    private function createEmptyPart() 
    {
        return [ 
            'content' => null, 
            'content_type' => null, 
            'content_transfer_encoding' => null
            ];
    }
}
