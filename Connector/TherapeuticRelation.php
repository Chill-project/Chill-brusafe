<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Connector;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TherapeuticRelation
{
    const STATE_NONE = "NONE";
    const STATE_AWAITING_APPROVAL = "AWAITING_APPROVAL";
    const STATE_NO_MEDICAL_DATA_SHARING_CONSENT = "NO_MEDICAL_DATA_SHARING_CONSENT";
    const STATE_OK = "OK";
    
    const PATIENT_ID = 'urn:oasis:names:tc:xacml:2.0:resource:resource-id';
    
    const AUTHOR_NAME = 'urn:oasis:names:tc:xspa:1.0:subject:subject-id';
    
    const AUTHOR_ROLE = "urn:oasis:names:tc:xacml:2.0:subject:role";
    
    const AUTHOR_ID = "urn:oasis:names:tc:xspa:1.0:subject:npi";
    
    const XMLNS_HL7_V3 = "urn:hl7-org:v3";
    
    const XMLNS_SAML = 'urn:oasis:names:tc:SAML:2.0:assertion';
    
    /**
     *
     * @var string
     */
    protected $patientId;
    
    /**
     *
     * @var string
     */
    protected $authorName;
    
    /**
     *
     * @var string
     */
    protected $authorId;
    
    /**
     *
     * @var string
     */
    protected $authorRole = "";
    
    /**
     * The whole saml assertion as document
     *
     * @var \DOMDocument
     */
    protected $saml;
    
    /**
     * The string representation of saml
     *
     * @var string
     */
    protected $samlString;
    
    
    public function __construct($saml)
    {
        $this->processSaml($saml);
    }
    
    protected function processSaml($saml)
    {
        $this->samlString = $saml;
        $doc = $this->saml = new \DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->loadXML($saml);

        $xpath = new \DOMXPath($doc);
        $xpath->registerNamespace('saml', self::XMLNS_SAML);
        $xpath->registerNamespace('hl7', self::XMLNS_HL7_V3);
        
        // get patient id
        $patientIdNodeList = $xpath->query(
            sprintf("//saml:Attribute[@Name='%s']",
                self::PATIENT_ID
                ))
            ;
        if ($patientIdNodeList->length === 0) {
            throw new \RuntimeException("the patient id node is not found");
        }
        $this->patientId = $patientIdNodeList->item(0)->nodeValue;
        
        // get author name
        $authorNameNodeList = $xpath->query(
            sprintf("//saml:Attribute[@Name='%s']", 
                self::AUTHOR_NAME))
            ;
        
        if ($authorNameNodeList->length === 0) {
            throw new \RuntimeException("the author node is not found");
        }
        $this->authorName = $authorNameNodeList->item(0)->nodeValue;
        
        
        $authorRoleNodeList = $xpath->query(
            sprintf("//saml:Attribute[@Name='%s']/saml:AttributeValue/hl7:Role/@displayName",
                self::AUTHOR_ROLE))
            ;
        
        if ($authorRoleNodeList->length === 0) {
            throw new \RuntimeException("the author role is not found");
        }
        $this->authorRole = $authorRoleNodeList
            ->item(0)
            ->nodeValue
            ;
        
        // get author id
        $authorIdNodeList = $xpath->query(
            sprintf(
                "//saml:Attribute[@Name='%s']/saml:AttributeValue/hl7:Role/@code",
                self::AUTHOR_ID
                ))
            ;
        if ($authorIdNodeList->length !== 1) {
            throw new \RuntimeException("the author id is not found in saml. "
                .$authorIdNodeList->length." elements found in saml \n\n"
                .$saml);
        }
        $this->authorId = $authorIdNodeList->item(0)->nodeValue;
        
    }
    
    /**
     * get saml assertion as \DOMDocument
     * 
     * @return \DOMDocument
     */
    public function getSamlAssertion()
    {
        return $this->saml;
    }
    
    public function getSamlString()
    {
        return $this->samlString;
    }
    
    /**
     * return string
     * 
     * @return string
     */
    public function getPatientId()
    {
        return $this->patientId;
    }
    
    public function getPatientIdAsCX()
    {
        // get the non blank parts
        $parts = \array_filter(\explode("&", $this->getPatientId()), function($p) {
            return !empty($p);
        });
        
        // replace "belgian governement" by 1.3.6.1.4.1.21297.100.1.1
        switch ($parts[1]) {
            case "Belgian Governement":
                $nonEmpty[1] = "1.3.6.1.4.1.21297.100.1.1";
                break;
            default:
                // do nothing
        }
        
        // return the patient id as cx :
        return sprintf("%s&%s&ISO", $parts[0], $parts[1]);
    }
    
    /**
     * get the name of the author
     * 
     * @return string
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * get the display name of the role
     * 
     * @return string
     */
    public function getAuthorRole()
    {
        return $this->authorRole;
    }
    
    /**
     * get the id of the author
     * 
     * @return string
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }


}
