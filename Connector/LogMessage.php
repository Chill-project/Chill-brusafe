<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Connector;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LogMessage
{
    /**
     *
     * @var \DOMDocument
     */
    protected $doc;
    
    /**
     *
     * @var \DOMElement
     */
    protected $audit_message;
    
    public function __construct()
    {
        $this->doc = $doc = new \DOMDocument('1.0');
        $this->audit_message = $auditMessage = $doc->createElement('AuditMessage');
        $auditMessage->setAttributeNS(
            'http://www.w3.org/2000/xmlns/', 
            'xmlns:xsi', 
            "http://www.w3.org/2001/XMLSchema-instance");
        $auditMessage->setAttributeNS(
            'http://www.w3.org/2000/xmlns/', 
            'xmlns:xsd',
            "http://www.w3.org/2001/XMLSchema");
        $doc->appendChild($auditMessage);
        
    }
    
    /**
     * 
     * @return \DomDocument
     */
    public function getDocument()
    {
        return $this->doc;
    }
    
    /**
     * Create the `EventIdentification` element. The element is returned for
     * a further customization.
     * 
     * The created element will be : 
     * 
     * ```
     * <EventIdentification EventActionCode="R" EventDateTime="2015-10-11T16:26:05.717Z"
     * EventOutcomeIndicator="0">
     *   <EventID csd-code="110106" codeSystemName="DCM" originalText="Export"/>
     *   <EventTypeCode csd-code="ITI-41" codeSystemName="IHE Transactions" 
     *       originalText="Provide and Register Document Set-b"/>
     * </EventIdentification>
     * ``
     * 
     * 
     * @param int|string $eventActionCode
     * @param \DateTime $datetime
     * @param string|int $eventIDDCMCode
     * @param string $eventIDOriginalText
     * @param string $eventTypeCodeCode
     * @param string $eventTypeCodeOriginalText
     * @return \DOMElement
     */
    public function addEventIdentification(
        $eventActionCode,
        \DateTime $datetime,
        $eventIDDCMCode,
        $eventIDOriginalText,
        $eventTypeCodeCode,
        $eventTypeCodeOriginalText
        )
    {
        $e = $this->doc->createElement('EventIdentification');
        $e->setAttribute("EventActionCode", $eventActionCode);
        $e->setAttribute("EventDateTime", $datetime->format(\DateTime::RFC3339));
        $e->setAttribute("EventOutcomeIndicator", "0");
        $e->appendChild($this->createCodeElement('EventID', $eventIDDCMCode, 
            'DCM', $eventIDOriginalText));
        $e->appendChild($this->createCodeElement('EventTypeCode', $eventTypeCodeCode,
            'IHE Transactions', $eventTypeCodeOriginalText));
        $this->audit_message->appendChild($e);
        
        return $e;
    }
    
    /**
     * Add a basic `ActiveParticipant` element to the log message.
     * 
     * The `DOMElement` is returned for a further customization.
     * 
     * XML representation of created element :
     * 
     * ```
     * <ActiveParticipant 
     *    UserID="$userID"
     *    AlternativeUserID="$alternativeUserID" >
     *    <RoleIDCode csd-code="$rodeIDCodeCode" 
     *       codeSystemName="$roleIDCodeSystemName" 
     *       originalText="$roleIDCodeOriginalText"/>
     * </ActiveParticipant>
     * 
     * ```
     * 
     * @param string $userID
     * @param string $alternativeUserID
     * @param string $roleIDCodeCode
     * @param string $roleIDCodeSystemName
     * @param string $roleIDCodeoriginalText
     * @return \DOMElement
     */
    public function addActiveParticipant(
        $userID,
        $alternativeUserID,
        $roleIDCodeCode,
        $roleIDCodeSystemName,
        $roleIDCodeoriginalText
    ) {
        /*
        <ActiveParticipant 
        UserID="2"
        AlternativeUserID="Ser Zomb" 
        >
        <!-- this information is retrieved from the SAML assertion -->
        <RoleIDCode csd-code="112247003" codeSystemName="SNOMED_CT" originalText="doctor"/>
    </ActiveParticipant>
         */
        
        $a = $this->doc->createElement("ActiveParticipant");
        $a->setAttribute("UserID", $userID);
        $a->setAttribute("AlternativeUserID", $alternativeUserID);
        
        $a->appendChild($this->createCodeElement('RoleIDCode', $roleIDCodeCode, 
            $roleIDCodeSystemName, $roleIDCodeoriginalText));
        
        $this->audit_message->appendChild($a);
        
        return $a;
    }
    
    /**
     * Add an `AuditSourceIdentification` element to the log message.
     * 
     * The element is returned for a further customization.
     * 
     * Created element :
     * 
     * ```
     * <AuditSourceIdentification AuditSourceID="$auditSouceId"  />
     * ```
     * 
     * @param string $auditSourceId
     * @return \DOMElement
     */
    public function addAuditSourceIdentification($auditSourceId)
    {
        /*
        <AuditSourceIdentification AuditSourceID="mass.chill.pro"  />
         */
        
        $a = $this->doc->createElement('AuditSourceIdentification');
        $a->setAttribute('AuditSourceID', $auditSourceId);
        
        $this->audit_message->appendChild($a);
        
        return $a;
    }
    
    public function addParticipantObjectIdentification(
        $participantObjectID,
        $participanObjectTypeCode,
        $participantObjectTypeCodeRole,
        \DOMElement $child = null
    ) {
        /*
            <ParticipantObjectIdentification 
        ParticipantObjectID="1234523651^^^&amp;2.16.840.1.113883.3.2591.600.1.0.1.1&amp;ISO"
        ParticipantObjectTypeCode="1" 
        ParticipantObjectTypeCodeRole="1"
        >
              <ParticipantObjectIDTypeCode csd-code="2"
                                   codeSystemName="RFC-3881"
                                   displayName="Patient Number"
                                   originalText="Patient Number"/>
    </ParticipantObjectIdentification>
         */
        
        $p = $this->doc->createElement('ParticipantObjectIdentification');
        $p->setAttribute('ParticipantObjectID', $participantObjectID);
        $p->setAttribute('ParticipantObjectTypeCode', $participanObjectTypeCode);
        $p->setAttribute('ParticipantObjectTypeCodeRole', 
            $participantObjectTypeCodeRole);
        
        
        
        if ($child !== null) {
            $p->appendChild($child);
        }
        
        $this->audit_message->appendChild($p);
        
        return $p;
    }
        
    
    /**
     * Create an element `$name` with an attribute `csd-code`,
     * `codeSystemName` and `originalText`.
     * 
     * @param \DOMDocument $doc
     * @param string $name
     * @param string $csd_code
     * @param string $codeSystemName
     * @param string $originalText
     * @return \DOMElement
     */
    public function createCodeElement(
        $name,
        $csd_code,
        $codeSystemName,
        $originalText
    ) {
        $el = $this->doc->createElement($name);
        $el->setAttribute('csd-code', $csd_code);
        $el->setAttribute('codeSystemName', $codeSystemName);
        $el->setAttribute('originalText', $originalText);
        
        return $el;
    }
}
