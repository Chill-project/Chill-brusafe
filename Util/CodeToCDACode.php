<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Chill\BrusafeBundle\Util;

use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\HealthBundle\Entity\Code;
use PHPHealth\CDA\DataType\Code\CodedValue;

/**
 * Transforma a "Chill code" (Chill\HealthBundle\Entity\Code)
 * to a "CDA code".
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CodeToCDACode
{
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }
    
    public function toCode(Code $code)
    {
        return new CodedValue(
                $code->getCode(),
                $this->translatableStringHelper->localize($code->getDisplayName()),
                $code->getCodeSystem(),
                $this->getCodeSystemName($code->getCodeSystem())
            );
    }
    
    protected function getCodeSystemName($codeSystem) 
    {
        switch ($codeSystem) {
            case '2.16.840.1.113883.6.96':
                return "SNOMED CT";
            case '2.16.840.1.113883.6.1':
                return "LOINC";
            default:
                return "";
        }
    }
}
