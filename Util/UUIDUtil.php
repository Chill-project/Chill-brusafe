<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Util;

use Doctrine\DBAL\Driver\Connection;

/**
 * Utility class to generate, create, and convert UUID
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class UUIDUtil
{
    /**
     *
     * @var \Doctrine\DBAL\Driver\Connection
     */
    protected $connection;
    
    /**
     * Prefix to convert oid to uuid
     *
     * @var string
     */
    protected $UUID_to_OID_prefix = '2.25';
    
    
    public function __construct(
        Connection $connection,
        $UUID_to_OID_prefix = null
    ) {
      $this->connection = $connection;
      
      if ($UUID_to_OID_prefix !== null) {
        $this->UUID_to_OID_prefix = $UUID_to_OID_prefix;
      }
    }
    
    /**
     * Return an uuid on the format `urn:uuid:$uuid`
     * 
     * @param string $uuid
     * @return string
     */
    public function toUrnUUID($uuid)
    {
        return 'urn:uuid:'.$uuid;
    }
    
    /**
     * Create an uuid
     * 
     * @param boolean $prefixUrn true if the uuid must be prefixed with `urn:uuid:`
     * @return string
     */
    public function createUUID($prefixUrn = false)
    {
        $stmt = $this->connection->query("SELECT UUID_GENERATE_V4()");
        
        return $prefixUrn ?
            $this->toUrnUUID($stmt->fetchColumn())
            :
            $stmt->fetchColumn()
            ;
    }
    
    /**
     * 
     * @param string $uuid
     * @return string
     */
    public function UUIDtoOID($uuid)
    {
        return $this->UUID_to_OID_prefix. (string) \base_convert(
            \str_replace("-", "", $uuid), 16, 10);
    }
}
