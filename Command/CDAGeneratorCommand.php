<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Generate a CDA file 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CDAGeneratorCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('chill:brusafe:debug:generate-cda')
            ->setDescription('Generate a CDA file')
            ->setHelp('Generate a samble cda file for a given person');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("executing command");
        $generator = new \Chill\BrusafeBundle\CDA\Generator\BrusafeReferralSummaryGenerator();
        
        $person = $this->getContainer()->get('chill.person.repository.person')
            ->findAll()[0];
        
        $doc = $generator->generateXml(array(
            'title' => 'Referral summary',
            'document_date'  => new \DateTime(),
            'document_id'    => [ '1234', 'https://mass.chill.pro' ],
            'document_language' => 'fr_FR',
            'person'         => $person
        ));
        
        $output->write($doc->saveXML());
    }
}
