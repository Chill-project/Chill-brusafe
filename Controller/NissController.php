<?php

namespace Chill\BrusafeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Chill\BrusafeBundle\Entity\Niss;
use Chill\PersonBundle\Security\Authorization\PersonVoter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class NissController extends Controller
{
    
    public function showAction($person_id)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $niss = $this->getDoctrine()->getManager()
            ->getRepository(Niss::class)
            ->findOneBy(['person' => $person])
            ;
        
        if ($niss === null) {
            $this->addFlash('notice', $this->get('translator')
                ->trans("The NISS does not exists for this person. Please "
                    . "insert it."));
            
            return $this->redirectToRoute('chill_brusafe_niss_form', [
                'person_id' => $person->getId()
            ]);
        }
        
        return $this->render('ChillBrusafeBundle:Niss:show.html.twig', array(
                'niss' => $niss
            ));
    }

    public function formAction($_locale, $person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $niss = $this->getDoctrine()->getManager()
            ->getRepository(Niss::class)
            ->findOneBy(['person' => $person])
            ;
        
        $form = $this->createUpsertForm($person, $niss, $request->query
            ->get('returnPath', null));
        
        return $this->render('ChillBrusafeBundle:Niss:upsert.html.twig',
            [
                'form' => $form->createView(),
                'person' => $person,
                'niss' => $niss
            ]);
    }
    
    public function upsertAction($person_id, Request $request)
    {
        /* @var $person \Chill\PersonBundle\Entity\Person */
        $person = $this->get('chill.person.repository.person')
            ->find($person_id);
        
        if ($person === null) {
            throw $this->createNotFoundException("The person is not found");
        }
        
        $this->denyAccessUnlessGranted(PersonVoter::SEE, $person);
        
        $niss = $this->getDoctrine()->getManager()
            ->getRepository(Niss::class)
            ->findOneBy(['person' => $person])
            ;
        
        $form = $this->createUpsertForm($person, $niss)
            ->handleRequest($request);
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            if ($em->contains($form->getData()) === false) {
                $em->persist($form->getData());
            }
            
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')
                ->trans("The new NISS is recorded"));
            
            if ($request->query->has('returnPath')) {
                return $this->redirect($request->query->get('returnPath'));
            }
            
            return $this->redirectToRoute('chill_brusafe_niss_show', 
                [ 'person_id' => $person->getId() ]);
        }
        
        return $this->render('ChillBrusafeBundle:Niss:upsert.html.twig',
            [
                'form' => $form->createView(),
                'person' => $person,
                'niss' => $niss
            ]);
    }
    
    protected function createUpsertForm(
        \Chill\PersonBundle\Entity\Person $person,
        Niss $niss = null,
        $returnPath = null
    ) {
        $form = $this->createForm(
            \Chill\BrusafeBundle\Form\NissType::class, 
            $niss === null ? (new Niss())->setPerson($person) : $niss, 
            [
                'action' => $this->generateUrl('chill_brusafe_niss_upsert',
                    [ 
                        'person_id' => $person->getId(),
                        'returnPath' => $returnPath
                    ])
            ]);
        
        $form->add('submit', SubmitType::class);
        
        return $form;
    }
}
