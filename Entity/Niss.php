<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Entity;

use Chill\PersonBundle\Entity\Person;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class Niss
{
    /**
     *
     * @var string
     */
    private $niss;
    
    /**
     *
     * @var int
     */
    private $id;
    
    /**
     *
     * @var \Chill\PersonBundle\Entity\Person
     */
    private $person;
    
    public function getNiss()
    {
        return $this->niss;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPerson()
    {
        return $this->person;
    }

    public function setNiss($niss)
    {
        $this->niss = $niss;
        
        return $this;
    }

    public function setPerson(Person $person)
    {
        $this->person = $person;
        
        return $this;
    }


}
