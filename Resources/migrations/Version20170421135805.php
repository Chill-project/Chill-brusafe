<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Create the NISS table, sequence, and constraints
 */
class Version20170421135805 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE '
            . 'chill_brusafe_niss_id_seq '
            . 'INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_brusafe_niss ('
            . 'id INT NOT NULL, '
            . 'person_id INT DEFAULT NULL, '
            . 'niss VARCHAR(15) NOT NULL, '
            . 'PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX '
            . 'UNIQ_68559CBD70A5A530 ON chill_brusafe_niss (niss)');
        $this->addSql('CREATE UNIQUE INDEX '
            . 'UNIQ_68559CBD217BBB47 ON chill_brusafe_niss (person_id)');
        $this->addSql('ALTER TABLE chill_brusafe_niss '
            . 'ADD CONSTRAINT FK_68559CBD217BBB47 '
            . 'FOREIGN KEY (person_id) '
            . 'REFERENCES chill_person_person (id) '
            . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE chill_brusafe_niss_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_brusafe_niss');
        
    }
}
