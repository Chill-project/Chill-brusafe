<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Chill\BrusafeBundle\Entity\Niss;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Chill\PersonBundle\Form\DataTransformer\PersonToIdTransformer;

/**
 * Form to cdreate / update a NISS
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class NissType extends AbstractType
{
    /**
     *
     * @var ObjectManager
     */
    protected $om;
    
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('niss', TextType::class)
            ->add('person', HiddenType::class)
            ;
        
        $builder
            ->get('person')
            ->addModelTransformer(new PersonToIdTransformer($this->om))
            ;
    }
    
    public function configureOptions(\Symfony\Component\OptionsResolver\OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Niss::class);
    }
}
