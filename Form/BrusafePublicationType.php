<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Entity\Center;
use Chill\HealthBundle\Form\DataTransformer\ConsultationTransformer;
use Chill\MainBundle\Form\Type\ChillDateType;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Chill\HealthBundle\Entity\Medication;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\HealthBundle\Entity\Publication;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Entity\User;

/**
 * Options : 
 * 
 * - consultation: the consultation about create 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class BrusafePublicationType extends AbstractType
{
    use \Chill\MainBundle\Form\Type\AppendScopeChoiceTypeTrait;
    
    /**
     *
     * @var User
     */
    protected $user;
    
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     *
     * @var EntityManager
     */
    protected $om;
    
    /**
     *
     * @var ConsultationTransformer
     */
    protected $consultationTransformer;
    
    function __construct(
        TokenStorageInterface $tokenStorage, 
        AuthorizationHelper $authorizationStringHelper, 
        EntityManager $om,
        TranslatableStringHelper $translatableStringHelper,
        ConsultationTransformer $consultationTransformer
    ) {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->authorizationHelper = $authorizationStringHelper;
        $this->om = $om;
        $this->translatableStringHelper = $translatableStringHelper;
        $this->consultationTransformer  = $consultationTransformer;
    }

    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /* @var $center Center */
        $center = $options['center'];
        
        $builder
            ->add('date', ChillDateType::class)
            ->add('author')
            ->add('consultation', HiddenType::class)
            ;
                
        $builder->get('consultation')
            ->addModelTransformer($this->consultationTransformer)
            ;
        
        $this->appendScopeChoices(
            $builder, 
            $options['role'], 
            $center,
            $this->user,
            $this->authorizationHelper,
            $this->translatableStringHelper,
            $this->om
            );
        
        $builder->addEventListener(
            FormEvents::PRE_SET_DATA, function(FormEvent $event) {
                $this->addMedicationField($event->getForm(), $event->getData());
            }
            );
        $builder->get('consultation')->addEventListener(
            FormEvents::POST_SUBMIT,   function(FormEvent $event) {
                $this->addMedicationField(
                    $event->getForm()->getParent(), 
                    $event->getForm()->getParent()->getData());
            }
            );
            
    }
    
    protected function addMedicationField(
        FormInterface $form, 
        Publication $publication
    ) {
        $consultation = $publication->getConsultation();
        
        if ($consultation === null) {
            $choices = array();
        } else {
            $choices = $publication
                ->getConsultation()
                ->getMedications()
                ->filter(function(Medication $m) {
                    return $m->isPublishable();
                });
        }

        $form->add('medications', EntityType::class, [
            'class' => Medication::class,
            'choice_label' => function(Medication $m) {
                return $this->translatableStringHelper->localize(
                    $m->getSubstance()->getDisplayName()
                    )
                    .' '.$m->getDoseQuantityValue()
                    .' '.$m->getDoseQuantityUnit()
                    ;
            },
            'choices'  => $choices,
            'multiple' => true,
            'expanded' => true
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('center')
            ->setAllowedTypes('center', [ Center::class ])
            ->setRequired('role')
            ->setAllowedTypes('role', [ Role::class ])
            ;
    }

}
