<?php

namespace Chill\BrusafeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

/**
 * Configure ChillBrusafe bundle
 */
class ChillBrusafeExtension extends Extension 
    implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $this->dispatchParameters($container, $config);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/connector.yml');
        $loader->load('services/form.yml');
        $loader->load('services/generator.yml');
        $loader->load('services/util.yml');
        $loader->load('services/repositories.yml');
    }
    
    protected function dispatchParameters(
        ContainerBuilder $container, 
        $config
    ) {
        foreach(['location', 'client_id', 'secret'] as $key) {
            $container->setParameter('chill_brusafe.api.'.$key, $config['api'][$key]);
        }
        
        foreach(['cert_file', 'endpoint'] as $key) {
            $container->setParameter('chill_brusafe.iti_41.'.$key, $config['iti_41'][$key]);
        }
        
        foreach(['source_id'] as $key) {
            $container->setParameter('chill_brusafe.xds.'.$key, $config['xds'][$key]);
        }
        
        foreach(['id', 'name'] as $key) {
            $container->setParameter('chill_brusafe.cda.custodian.'.$key, 
                $config['cda']['custodian'][$key]);
        }
        
        $container->setParameter('chill_brusafe.cda.oid.extension',
            $config['cda']['oid']['extension']);
        
        // iti 20
        foreach(['source_machine', 'endpoint'] as $key) {
            $container->setParameter('chill_brusafe.iti_20.'.$key,
                $config['iti_20'][$key]);
        }
    }
    
    public function prepend(ContainerBuilder $container)
    {
        $this->prependRoutes($container);
    }
    
    public function prependRoutes(ContainerBuilder $container) 
        {
            //add routes for custom bundle
             $container->prependExtensionConfig('chill_main', array(
               'routing' => array(
                  'resources' => array(
                     '@ChillBrusafeBundle/Resources/config/routing.yml'
                  )
               )
            ));
        }
}
