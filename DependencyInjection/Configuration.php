<?php

namespace Chill\BrusafeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('chill_brusafe');

        $rootNode
            ->children()
                ->arrayNode('api')
                    ->info("The parameters to connect to brusafe+ api")
                    ->children()
                        ->scalarNode('location')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("The url of the api")
                        ->end()
                        ->scalarNode('client_id')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("the client id given by brusafe+ network")
                        ->end()
                        ->scalarNode('secret')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("The secret (password) given by brusafe+ network")
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('iti_41')
                    ->info("The parameters for transaction IHE ITI-41 (Provide and "
                        . "register document set)")
                    ->children()
                        ->scalarNode('cert_file')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("the url of the cert file. Must contains the cert and private key")
                        ->end()
                        ->scalarNode('endpoint')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("The url of the ITI 41 endpoint")
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('iti_20')
                    ->isRequired()
                    ->info("The parameters for transaction ITI-20 (Record Audit Event)")
                    ->children()
                        ->scalarNode('endpoint')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("the audit endpoint, expressed as a full url")
                            ->example("tcps://api.qa.brusafe.be:6514")
                        ->end()
                        ->scalarNode('source_machine')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("the url (DNS) of the source machine")
                            ->example('mass.chill.pro')
                        ->end()
                    ->end() // end of iti_20.children
                ->end() // end of iti_20
                ->arrayNode('xds')
                    ->isRequired()
                    ->info('Information to build XDS metadata')
                    ->children()
                        ->scalarNode('source_id')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info("the OID of the source id")
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('cda')
                    ->info('Information to build CDA documents')
                    ->children()
                        ->arrayNode('custodian')
                            ->info("Information about the custodian")
                            ->children()
                                ->scalarNode('id')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->info("the id of the custodian")
                                ->end()
                                ->scalarNode('name')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->info("the name of the custodian")
                                ->end()
                            ->end() // end of custodian's children
                        ->end() // end of custodian
                        ->arrayNode('oid')
                            ->info("Info about oid plan")
                            ->children()
                                ->scalarNode('extension')
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                    ->info("the extension for oid")
                                ->end()
                            ->end() // end of oid's children
                        ->end() // end of oid
                    ->end() // end of cda's children
                ->end() // end of cda
            ->end()
            ;

        return $treeBuilder;
    }
}
