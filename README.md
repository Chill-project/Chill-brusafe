Brusafe Bundle for Chill software
=================================

The whole chill documentation can be found at http://docs.chill.social

This bundle works with other bundles. We will prepare a docker container to have a ready-to-work instance.

For people working with Brusafe
-------------------------------

The most insteresting part should be in the directory `Connector` :

- `ProvideDocumentBody` (must be renamed) prepare the XDS metadata ;
- `APIConnector` connect to the api to retrieve and create assertion. This can be seen in action in the controller `Controller/ConnectController` and `Controller/PublicationController::postDocumentAction`
- `ITI41ProvideAndRegisterDocument` make the soap call to provide and register document (ITI-41)

