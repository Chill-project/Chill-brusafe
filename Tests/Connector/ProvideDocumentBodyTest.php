<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Tests\Connector;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Chill\BrusafeBundle\Connector\ProvideDocumentBody;
use Chill\HealthBundle\Entity\Publication;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ProvideDocumentBodyTest extends KernelTestCase
{
    
    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     *
     * @var BrusafeReferralSummaryGenerator
     */
    protected $generator;
    
    /**
     *
     * @var ProvideDocumentBody
     */
    protected $provideDocument;


    public function setUp()
    {
        self::bootKernel();
        
        $this->entityManager = self::$kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ;
        
        $this->generator = self::$kernel
            ->getContainer()
            ->get('chill_brusafe.abrumet_referral_summary_generator')
            ;
        
        $this->provideDocument = self::$kernel
            ->getContainer()
            ->get('chill_brusafe.provide_document')
            ;
        
        // add a fake request with a default locale (used in translatable string)
        $prophet = new \Prophecy\Prophet;
        $request = $prophet->prophesize();
        $request->willExtend(\Symfony\Component\HttpFoundation\Request::class);
        $request->getLocale()->willReturn('fr');
        
        self::$kernel
            ->getContainer()
            ->get('request_stack')
            ->push($request->reveal());
    }
    
    public function testcreatePublish()
    {
        $body = $this->provideDocument;
        $body->addDocument($this->createPublication());
        $body->setTherapeuticLink($this->createTherapeuticLink());
        
        list($dom, $attachments) = $body->createPublish();
        
        $dom->formatOutput = true;
        $dom->save(\sys_get_temp_dir().'/xds-iti-41.xml');
        
        // save all content
        $content = $dom->saveXML()."\r\n".$attachments;
        \file_put_contents(\sys_get_temp_dir().'/xds-iti-41.attachements.txt', 
            $content);
        
        $this->assertInstanceOf(\DOMDocument::class, $dom);
    }
    
    /**
     * @return Publication
     */
    protected function createPublication()
    {
        // get a consultation which contains medication
        $consultations = $this->entityManager
            ->createQuery(
                "SELECT c FROM ChillHealthBundle:Consultation c "
                . "JOIN c.medications m "
                . "WHERE SIZE(c.medications) > 0 "
                )
            ->getResult()
            ;
        
        if (count($consultations) === 0) {
            throw new \RuntimeException("There aren't any consultation available "
                . "to run this test");
        }
        
        /* @var $consultation Consultation */
        $consultation = $consultations[0];
        
        $publication = (new Publication())
            ->setAuthor($consultation->getAuthor())
            ->setPatient($consultation->getPatient())
            ->setDate($consultation->getDate())
            ->setPublicationType('brusafe_referral_summary')
            ;
        
        foreach ($consultation->getMedications() as $medication) {
            $publication->addMedication($medication);
        }
        
        $publication->setData($this->generator->generateXml($publication, 
            $this->createTherapeuticLink())->saveXML());
        
        // persist to set an id, but we do not flush the entity manager
        $this->entityManager->persist($publication);
        
        return $publication;
    }
    
    /**
     * 
     * @return \Chill\BrusafeBundle\Connector\TherapeuticRelation
     */
    protected function createTherapeuticLink()
    {
        return new \Chill\BrusafeBundle\Connector\TherapeuticRelation($this->xml);
    }
    
    protected $xml = <<<XML
<?xml version="1.0"?>
<saml:Assertion xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns="urn:oasis:names:tc:SAML:2.0:assertion" ID="ID_88a29ef0-010f-4965-99f6-f72a9ab4b8f4" IssueInstant="2017-04-16T13:45:29.000Z" Version="2.0">
  <saml:Issuer>Dioss identity provider</saml:Issuer>
  <dsig:Signature xmlns:dsig="http://www.w3.org/2000/09/xmldsig#">
    <dsig:SignedInfo>
      <dsig:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
      <dsig:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
      <dsig:Reference URI="#ID_88a29ef0-010f-4965-99f6-f72a9ab4b8f4">
        <dsig:Transforms>
          <dsig:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
          <dsig:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
        </dsig:Transforms>
        <dsig:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
        <dsig:DigestValue>w/e3JudhmiMX0g2zovfoW4NMLb/Mp7V+1YUtSU2PuuM=</dsig:DigestValue>
      </dsig:Reference>
    </dsig:SignedInfo>
    <dsig:SignatureValue>0NVP9KBQoltkiDkqLkgBOQ4R+Fkny5UqoJLFjp6WOonzvimnIkdJGHO8j3oOZowopbUJPg9hduqvzNCsTktvVkmPKfY3pPwPzPtwOASqxBVgDK1IGRVqLBfqvEU9WwxbXqEmf+IatcOtQ10+dED7HQMxZq2BiyokKlPGxOaEE2HlQKYUVGzLHGjGaLbp1/yO9gibb/OpYGjYi58Jg/5nLiV3l6QsFhdmRrLo42VEan+cHTrtZBxMGDeAbu5UInU/o2hlMqNLEPkfH5HquxhyjrGliy16nxkbmRKL8BNrOwXIEF0H2X9uZVb6kQ/POOB3tuNjJ113TozYGLD2yTXhYQ==</dsig:SignatureValue>
    <dsig:KeyInfo>
      <dsig:X509Data>
        <dsig:X509Certificate>MIIHdDCCBlygAwIBAgIMKOQAzBulzreM3XRNMA0GCSqGSIb3DQEBCwUAMGAxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9iYWxTaWduIG52LXNhMTYwNAYDVQQDEy1HbG9iYWxTaWduIERvbWFpbiBWYWxpZGF0aW9uIENBIC0gU0hBMjU2IC0gRzIwHhcNMTYwOTE5MTYwOTU2WhcNMTcwOTIwMTYwOTU2WjA9MSEwHwYDVQQLExhEb21haW4gQ29udHJvbCBWYWxpZGF0ZWQxGDAWBgNVBAMMDyoucWEuYnJ1c2FmZS5iZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOMgI8Ny6Uy6rEywjGveslub7XVkGK/XLK6jITDvKiJ2DABLr1QObBIZf9bokEGFkBhEbvSOH7Kmn8qH9ZIXRfQPAOdqy+iCJkav1/hFY5zF3YiIhuGuoFwYXCHTYgrk7dGQrXniIXHB7xNV9CRUGDyLVCyTBMrFXfZqSS/3dFUKAyQ8JzOadpYIW+K3a5n+AkPEToJFcV48sIpnfLiltbGso82jmJEOIHrXVfgszRi2prKh2UE80uHIPFjX9Ds2vxph3pTg7NESr/6TMxazcPtBG5rjSNnC9WBa/uiH8jp8lJlMKao8jliUphHaSpdVkTp8PSZ8heiUPDPzKesO0wMCAwEAAaOCBE8wggRLMA4GA1UdDwEB/wQEAwIFoDCBlAYIKwYBBQUHAQEEgYcwgYQwRwYIKwYBBQUHMAKGO2h0dHA6Ly9zZWN1cmUuZ2xvYmFsc2lnbi5jb20vY2FjZXJ0L2dzZG9tYWludmFsc2hhMmcycjEuY3J0MDkGCCsGAQUFBzABhi1odHRwOi8vb2NzcDIuZ2xvYmFsc2lnbi5jb20vZ3Nkb21haW52YWxzaGEyZzIwVgYDVR0gBE8wTTBBBgkrBgEEAaAyAQowNDAyBggrBgEFBQcCARYmaHR0cHM6Ly93d3cuZ2xvYmFsc2lnbi5jb20vcmVwb3NpdG9yeS8wCAYGZ4EMAQIBMAkGA1UdEwQCMAAwQwYDVR0fBDwwOjA4oDagNIYyaHR0cDovL2NybC5nbG9iYWxzaWduLmNvbS9ncy9nc2RvbWFpbnZhbHNoYTJnMi5jcmwwKQYDVR0RBCIwIIIPKi5xYS5icnVzYWZlLmJlgg1xYS5icnVzYWZlLmJlMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAdBgNVHQ4EFgQUfFarcVsG0x1Pst7DRKbH4hBgUlYwHwYDVR0jBBgwFoAU6k581IAt5RWBhiaMgm3AmKTPlw8wggJuBgorBgEEAdZ5AgQCBIICXgSCAloCWAB1AGj2mPgfZIK+OozuuSgdTPxxUV1nk9RE0QpnrLtPT/vEAAABV0M2DUwAAAQDAEYwRAIgG4eknUH519e6NTKYOEVo6Lg1u2Jnx8xlb5tkKgWPqOkCICv+6uwzMzsqCi1B8Mfq7FvZQc3je4rl2dYwsXRstPgpAHcApLkJkLQYWBSHuxOizGdwCjw1mAT5G9+443fNDsgN3BAAAAFXQzYTHwAABAMASDBGAiEA3tKQ37wRdlcEKQW0cSt6mmUQES5Sc4NguonkT4IhMPsCIQChkV2SRk0HpQxaRdU2mpFog7z0VH3biWeVz/f6nGOC0gB2AO5Lvbd1zmC64UJpH6vhnmajD35fsHLYgwDEe4l6qP3LAAABV0M2HDgAAAQDAEcwRQIgQt2xO8Ouu9dmieZzj4HoCqGfkrBg1hVHdLHqcXI2juYCIQDy4Mgu9tfz+igNKvceakpzRaiHpcHxACfGaXAjPAWY8wB3AN3rHSt6DU+mIIuBrYFocH4ujp0B1VyIjT0RxM227L7MAAABV0M2IKUAAAQDAEgwRgIhAJeRXMEmO5vIljaCTg41VwN2ohtNLOMiBh+Miw6aX0KmAiEA+mDiBJbXhYf+83EQeHM/pPREzNpvpjxr0FKHKwekAWUAdQBWFAaaL9fC7NP14b1Esj7HRna5vJkRXMDvlJhV1onQ3QAAAVdDNiRsAAAEAwBGMEQCIAWRGcBbp3DBKnllOQ7XFayEsQyY7V1UK07x3kee4ujNAiBmGaH1B/z7RhwYtc5h+d8PCZ98ye0/p21rbwdd85Sp4TANBgkqhkiG9w0BAQsFAAOCAQEAgPwS/KG+sunciHBP1PFDhDBc/qvo0Rj3DNsxFzHG9B0mUD9bGB2MTcLDePocS9ya4nJ1zHyi/tPoecsyt7bQnvapzZ3G2uha2acGt8yoKrAyH/ROKUL0aMHqIP9uA9qmIeURNV2Sq7+1ej+j5St/79ZYntd4eoXUMgKzECxEZZ/HeBworN7JtUxcM1ERyGVLczEjQkGCfqX1wXhO5Do167TdmskqJGhlt6NsUgz4wQmMz4uIOXeuj+RNWjFqyd1Ak73fOVUaGGNlBzCxXyAOa82Q4EOQxMT6qSAndUjEhruNt2Gl4V5mPQOMy/UmbckMq/PRxSxjqszPSZ+mRT2xNg==</dsig:X509Certificate>
      </dsig:X509Data>
      <dsig:KeyValue>
        <dsig:RSAKeyValue>
          <dsig:Modulus>4yAjw3LpTLqsTLCMa96yW5vtdWQYr9csrqMhMO8qInYMAEuvVA5sEhl/1uiQQYWQGERu9I4fsqafyof1khdF9A8A52rL6IImRq/X+EVjnMXdiIiG4a6gXBhcIdNiCuTt0ZCteeIhccHvE1X0JFQYPItULJMEysVd9mpJL/d0VQoDJDwnM5p2lghb4rdrmf4CQ8ROgkVxXjywimd8uKW1sayjzaOYkQ4getdV+CzNGLamsqHZQTzS4cg8WNf0Oza/GmHelODs0RKv/pMzFrNw+0EbmuNI2cL1YFr+6IfyOnyUmUwpqjyOWJSmEdpKl1WROnw9JnyF6JQ8M/Mp6w7TAw==</dsig:Modulus>
          <dsig:Exponent>AQAB</dsig:Exponent>
        </dsig:RSAKeyValue>
      </dsig:KeyValue>
    </dsig:KeyInfo>
  </dsig:Signature>
  <saml:Subject>
    <saml:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified">Julien Fastr&#xE9;</saml:NameID>
    <saml:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
      <saml:SubjectConfirmationData NotOnOrAfter="2017-04-16T13:45:29.000Z"/>
    </saml:SubjectConfirmation>
  </saml:Subject>
  <saml:Conditions NotBefore="2017-04-16T13:45:27.000Z" NotOnOrAfter="2017-04-16T15:25:27.000Z">
    <saml:AudienceRestriction>
      <saml:Audience>https://abrumet.be</saml:Audience>
    </saml:AudienceRestriction>
  </saml:Conditions>
  <saml:AuthnStatement AuthnInstant="2017-04-16T13:45:29.001Z" SessionIndex="ID_88a29ef0-010f-4965-99f6-f72a9ab4b8f4">
    <saml:AuthnContext>
      <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml:AuthnContextClassRef>
    </saml:AuthnContext>
  </saml:AuthnStatement>
  <saml:AttributeStatement>
    <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Julien Fastr&#xE9;</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:npi">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">80061626918</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute FriendlyName="Patient Privacy Policy Acknowledgement Document" Name="urn:ihe:iti:bppc:2007:docid" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:anyURI">urn:oid:1.3.6.1.4.1.48336.1.2.11.2.61092100976</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">61092100976^^^Belgian Governement^NNBEL</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute FriendlyName="Patient Relationship" Name="urn:enovation:xua:2016:patientRelationship" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">true</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role">
      <saml:AttributeValue>
        <Role xmlns="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="112247003" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="doctor" xsi:type="CE"/>
      </saml:AttributeValue>
    </saml:Attribute>
  </saml:AttributeStatement>
</saml:Assertion>
XML;
    
}
