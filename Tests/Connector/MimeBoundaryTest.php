<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Tests\Connector;

use Chill\BrusafeBundle\Connector\MimeBoundary;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MimeBoundaryTest extends \PHPUnit\Framework\TestCase
{
    public function testClass()
    {
        $mime = new MimeBoundary($this->content);
        
        $this->assertStringStartsWith(
            '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">', 
            $mime->getPartContent('<root.message@cxf.apache.org>')
            );
    }
    
    public function testGetStartContentId()
    {
        $this->assertEquals(
            "<root.message@cxf.apache.org>",
            MimeBoundary::getStartContentId($this->contentTypeHeader)
            );
    }
    
    private $contentTypeHeader = ['multipart/related; type="application/xop+xml"; boundary="uuid:9c598a25-af2e-47d8-9626-70e3efb02091"; start="<root.message@cxf.apache.org>"; start-info="application/soap+xml"'];
    
    private $content = <<<CONTENT
--uuid:9c598a25-af2e-47d8-9626-70e3efb02091
Content-Type: application/xop+xml; charset=UTF-8; type="application/soap+xml"
Content-Transfer-Encoding: binary
Content-ID: <root.message@cxf.apache.org>

<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"><soap:Header><Action xmlns="http://schemas.xmlsoap.org/ws/2004/08/addressing">urn:ihe:iti:xds-b:2007:DocumentRepository_PortType:DocumentRepository_ProvideAndRegisterDocumentSet-b:Fault:WSSecurityException</Action><MessageID xmlns="http://schemas.xmlsoap.org/ws/2004/08/addressing">urn:uuid:672c3d11-287f-4040-8301-07911d5566ff</MessageID><To xmlns="http://schemas.xmlsoap.org/ws/2004/08/addressing">http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous</To><RelatesTo xmlns="http://schemas.xmlsoap.org/ws/2004/08/addressing">urn:uuid:d65882c9-d80f-4651-a2b7-b14f46c5c944</RelatesTo></soap:Header><soap:Body><soap:Fault><soap:Code><soap:Value>soap:Sender</soap:Value><soap:Subcode><soap:Value xmlns:ns1="http://ws.apache.org/wss4j">ns1:SecurityError</soap:Value></soap:Subcode></soap:Code><soap:Reason><soap:Text xml:lang="en">A security error was encountered when verifying the message</soap:Text></soap:Reason></soap:Fault></soap:Body></soap:Envelope>
--uuid:9c598a25-af2e-47d8-9626-70e3efb02091--
CONTENT;
}
