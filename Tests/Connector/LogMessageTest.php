<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Tests\Connector;

use PHPUnit\Framework\TestCase;
use Chill\BrusafeBundle\Connector\LogMessage;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LogMessageTest extends TestCase
{
    public function testGenerate()
    {
        $expectedXML = <<<XML
<AuditMessage xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <EventIdentification EventActionCode="R" EventDateTime="2015-10-11T16:26:05Z"
        EventOutcomeIndicator="0">
        <EventID csd-code="110106" codeSystemName="DCM" originalText="Export"/>
        <EventTypeCode csd-code="ITI-41" codeSystemName="IHE Transactions"
            originalText="Provide and Register Document Set-b"/>
    </EventIdentification>
    
    
    <!-- the source of the message -->
    <!-- UserID == process PID -->
    <ActiveParticipant  
        UserID="15"
        AlternativeUserID="mass.chill.pro"
        UserIsRequestor="true" 
        NetworkAccessPointTypeCode="1"
        NetworkAccessPointID="https://mass.chill.pro"
        >
        <RoleIDCode csd-code="110153" codeSystemName="DCM" originalText="Source"/>
    </ActiveParticipant>
    
    
    <!-- Human requestor (if known) -->
    <!-- the "Alternative User ID" is the name of the doctor, retrieved from the SAML -->
    <ActiveParticipant 
        UserID="2"
        AlternativeUserID="Ser Zomb" 
        UserIsRequestor="true"
        >
        <!-- this information is retrieved from the SAML assertion -->
        <RoleIDCode csd-code="112247003" codeSystemName="SNOMED_CT" originalText="doctor"/>
    </ActiveParticipant>

    
    <!-- Destination -->
    <ActiveParticipant
        UserID="https://api.qa.brusafe.be:8003/services/repository"
        AlternativeUserID="Brusafe+ network"
        UserIsRequestor="false"
        NetworkAccessPointTypeCode="1"
        NetworkAccessPointID="https://api.qa.brusafe.be"
        >
        <RoleIDCode csd-code="110152" codeSystemName="DCM" originalText="Destination"/>
    </ActiveParticipant>
    
    
    <!-- Audit source -->
    <AuditSourceIdentification AuditSourceID="mass.chill.pro"  />
    
    
    <!-- Patient -->
    <ParticipantObjectIdentification 
        ParticipantObjectID="1234523651^^^&amp;2.16.840.1.113883.3.2591.600.1.0.1.1&amp;ISO"
        ParticipantObjectTypeCode="1" 
        ParticipantObjectTypeCodeRole="1"
        >
              <ParticipantObjectIDTypeCode csd-code="2"
                                   codeSystemName="RFC-3881"
                                   displayName="Patient Number"
                                   originalText="Patient Number"/>
    </ParticipantObjectIdentification>
    
    
    <!-- Submission set -->
    <ParticipantObjectIdentification
        ParticipantObjectID="urn:uuid:701000d8-b22e-4015-b0ca-609e30cb1893"
        ParticipantObjectTypeCode="2" 
        ParticipantObjectTypeCodeRole="20"
        >
        <ParticipantObjectIDTypeCode
            csd-code="urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd"
            codeSystemName="IHE XDS Metadata"
            originalText="submission set classificationNode"
        />
    </ParticipantObjectIdentification>
</AuditMessage>
XML;
        $expectedDoc = new \DOMDocument('1.0');
        $expectedDoc->loadXML($expectedXML);
        
        $logMessage = new LogMessage();
        
        $logMessage->addEventIdentification(
            "R", 
            // the date of submission set
            \DateTime::createFromFormat(\DateTime::RFC3339, "2015-10-11T16:26:05Z"), 
            "110106", 
            "Export", 
            "ITI-41", 
            "Provide and Register Document Set-b");
        
        // source requestor
        $source = $logMessage->addActiveParticipant(
            // the pid of the process
            "15", 
            // the id of the source
            "mass.chill.pro", 
            "110153", 
            "DCM", 
            "Source");
        $source->setAttribute("UserIsRequestor", "true");
        // 1 if dns is used in NetworkAccessPointID, 
        $source->setAttribute("NetworkAccessPointTypeCode", "1");
        // address to reach the source machine
        $source->setAttribute("NetworkAccessPointID", "https://mass.chill.pro");
        
        // human requestor
        // only if known
        $human = $logMessage->addActiveParticipant(
            // id of the user
            "2",
            // name of the user who created the document
            "Ser Zomb",
            // check thos informations
            "112247003",
            "SNOMED_CT",
            "doctor");
        $human->setAttribute("UserIsRequestor", "true");
        
        // destination
        $dest = $logMessage->addActiveParticipant(
            // url of the endpoint
            "https://api.qa.brusafe.be:8003/services/repository",
            // name of the endpoint
            "Brusafe+ network", 
            "110152", 
            "DCM", 
            "Destination");
        // 1 if dns is used in NetworkAccessPointID,
        $dest->setAttribute("NetworkAccessPointTypeCode", "1");
        // address to reach the dest machine
        $dest->setAttribute("NetworkAccessPointID", "https://api.qa.brusafe.be");
        $dest->setAttribute("UserIsRequestor", "false");
        
        // audit source id
        $logMessage->addAuditSourceIdentification("mass.chill.pro");
        
        // patient
        $code = $logMessage->createCodeElement('ParticipantObjectIDTypeCode', 
            "2", "RFC-3881", "Patient Number");
        $code->setAttribute("displayName", "Patient Number");
        $logMessage->addParticipantObjectIdentification(
            "1234523651^^^&2.16.840.1.113883.3.2591.600.1.0.1.1&ISO", 
            "1", 
            "1",
            $code);
        
        // submission set
        $code = $logMessage->createCodeElement(
            'ParticipantObjectIDTypeCode',
            "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd",
            "IHE XDS Metadata",
            'submission set classificationNode'
            );
        $logMessage->addParticipantObjectIdentification(
            // uuid of the submission set
            "urn:uuid:701000d8-b22e-4015-b0ca-609e30cb1893", 
            "2", 
            "20",
            $code
            );
        
        
        // clone the document to dump a formatted xml representation
        $xml = clone $logMessage->getDocument();
        $xml->formatOutput = true;
        $xml->save(sys_get_temp_dir()."/".__METHOD__.".xml");
        
        $this->assertEqualXMLStructure(
            $expectedDoc->documentElement,
            $logMessage->getDocument()->documentElement,
            "test that the xml structure is equal"
            );
        
        
    }
}
