<?php
/*
 * Copyright (C) 2017 Champs Libres Cooperative <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Chill\BrusafeBundle\Tests\Connector;

use PHPUnit\Framework\TestCase;
use Chill\BrusafeBundle\Connector\TherapeuticRelation;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TherapeuticRelationTest extends TestCase
{
    
    public function testAuthorId()
    {
        foreach ([$this->saml] as $saml) {
            $tl = new TherapeuticRelation($saml);
            
            $this->assertEquals("123456789", $tl->getAuthorId());
        }
    }
    
    public function testPatientId()
    {
        $tl = new TherapeuticRelation($this->saml);
        
        $this->assertEquals('80061626923^^^&1.3.6.1.4.1.21297.100.1.1&ISO', 
            $tl->getPatientId());
        
    }
    
    public function testPatientIdCX()
    {
        $tl = new TherapeuticRelation($this->saml);
        
        $this->assertEquals('80061626923^^^&1.3.6.1.4.1.21297.100.1.1&ISO',
            $tl->getPatientIdAsCX());
        $this->assertEquals("80061626923^^^&amp;1.3.6.1.4.1.21297.100.1.1"
            . "&amp;ISO", \htmlspecialchars($tl->getPatientIdAsCX(), ENT_XML1));
    }
    
    public function testAuthorName()
    {
        $tl = new TherapeuticRelation($this->saml);
        
        $this->assertEquals("Julien Fastré", $tl->getAuthorName());
    }
    
    public function testAuthorRole()
    {
        foreach ([$this->saml ] as $saml ) {
            $tl = new TherapeuticRelation($saml);

            $this->assertEquals("doctor", $tl->getAuthorRole());
        }
    }
        
        protected $saml = <<<XML
<?xml version="1.0"?>
<saml:Assertion xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns="urn:oasis:names:tc:SAML:2.0:assertion" ID="ID_6f7f163b-c24b-4db2-8e27-5d33df49daae" IssueInstant="2017-05-12T08:36:26.500Z" Version="2.0">
  <saml:Issuer>Dioss identity provider</saml:Issuer>
  <dsig:Signature xmlns:dsig="http://www.w3.org/2000/09/xmldsig#">
    <dsig:SignedInfo>
      <dsig:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
      <dsig:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>
      <dsig:Reference URI="#ID_6f7f163b-c24b-4db2-8e27-5d33df49daae">
        <dsig:Transforms>
          <dsig:Transform Algorithm="http://www.w3.org/2000/09/xmldsig#enveloped-signature"/>
          <dsig:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
        </dsig:Transforms>
        <dsig:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>
        <dsig:DigestValue>XR31FtWCqI9V/IVFUQirycYPSCywwqeNzm6aIC3+EZI=</dsig:DigestValue>
      </dsig:Reference>
    </dsig:SignedInfo>
    <dsig:SignatureValue>CWrH1nZpsli82xQE1heq6fe/gq0c/jYxPSDtqoARvQg/Yn6HbWeT6ul/tv7dPE5Wmnjxeh78JJTxEXpkOEmjM8W92KGN0s9LMdMZXPfDL4UeIFKkfod+CK2i8NGA5/1VU41FvTSEtujZwYv6HT8pTFHkpsJkAKTeM+0EKOr0Up0cC1b/zp7TYqSoG5cvhONmqfr6abj2vcDI+CjZtu42DUCzhaI8nii9sxGxwcDNJ8P49P5Ghiczmm2kznXM3MkDVPjCipJ2XH2Rf8oAUNDLKfiBi0B84T9Uxq6zAItVBM1H6fBWUW36WCV09NVTB/Z9QX1g2pgo0v11DntqxaYlLA==</dsig:SignatureValue>
    <dsig:KeyInfo>
      <dsig:X509Data>
        <dsig:X509Certificate>MIIHdDCCBlygAwIBAgIMKOQAzBulzreM3XRNMA0GCSqGSIb3DQEBCwUAMGAxCzAJBgNVBAYTAkJFMRkwFwYDVQQKExBHbG9iYWxTaWduIG52LXNhMTYwNAYDVQQDEy1HbG9iYWxTaWduIERvbWFpbiBWYWxpZGF0aW9uIENBIC0gU0hBMjU2IC0gRzIwHhcNMTYwOTE5MTYwOTU2WhcNMTcwOTIwMTYwOTU2WjA9MSEwHwYDVQQLExhEb21haW4gQ29udHJvbCBWYWxpZGF0ZWQxGDAWBgNVBAMMDyoucWEuYnJ1c2FmZS5iZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOMgI8Ny6Uy6rEywjGveslub7XVkGK/XLK6jITDvKiJ2DABLr1QObBIZf9bokEGFkBhEbvSOH7Kmn8qH9ZIXRfQPAOdqy+iCJkav1/hFY5zF3YiIhuGuoFwYXCHTYgrk7dGQrXniIXHB7xNV9CRUGDyLVCyTBMrFXfZqSS/3dFUKAyQ8JzOadpYIW+K3a5n+AkPEToJFcV48sIpnfLiltbGso82jmJEOIHrXVfgszRi2prKh2UE80uHIPFjX9Ds2vxph3pTg7NESr/6TMxazcPtBG5rjSNnC9WBa/uiH8jp8lJlMKao8jliUphHaSpdVkTp8PSZ8heiUPDPzKesO0wMCAwEAAaOCBE8wggRLMA4GA1UdDwEB/wQEAwIFoDCBlAYIKwYBBQUHAQEEgYcwgYQwRwYIKwYBBQUHMAKGO2h0dHA6Ly9zZWN1cmUuZ2xvYmFsc2lnbi5jb20vY2FjZXJ0L2dzZG9tYWludmFsc2hhMmcycjEuY3J0MDkGCCsGAQUFBzABhi1odHRwOi8vb2NzcDIuZ2xvYmFsc2lnbi5jb20vZ3Nkb21haW52YWxzaGEyZzIwVgYDVR0gBE8wTTBBBgkrBgEEAaAyAQowNDAyBggrBgEFBQcCARYmaHR0cHM6Ly93d3cuZ2xvYmFsc2lnbi5jb20vcmVwb3NpdG9yeS8wCAYGZ4EMAQIBMAkGA1UdEwQCMAAwQwYDVR0fBDwwOjA4oDagNIYyaHR0cDovL2NybC5nbG9iYWxzaWduLmNvbS9ncy9nc2RvbWFpbnZhbHNoYTJnMi5jcmwwKQYDVR0RBCIwIIIPKi5xYS5icnVzYWZlLmJlgg1xYS5icnVzYWZlLmJlMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAdBgNVHQ4EFgQUfFarcVsG0x1Pst7DRKbH4hBgUlYwHwYDVR0jBBgwFoAU6k581IAt5RWBhiaMgm3AmKTPlw8wggJuBgorBgEEAdZ5AgQCBIICXgSCAloCWAB1AGj2mPgfZIK+OozuuSgdTPxxUV1nk9RE0QpnrLtPT/vEAAABV0M2DUwAAAQDAEYwRAIgG4eknUH519e6NTKYOEVo6Lg1u2Jnx8xlb5tkKgWPqOkCICv+6uwzMzsqCi1B8Mfq7FvZQc3je4rl2dYwsXRstPgpAHcApLkJkLQYWBSHuxOizGdwCjw1mAT5G9+443fNDsgN3BAAAAFXQzYTHwAABAMASDBGAiEA3tKQ37wRdlcEKQW0cSt6mmUQES5Sc4NguonkT4IhMPsCIQChkV2SRk0HpQxaRdU2mpFog7z0VH3biWeVz/f6nGOC0gB2AO5Lvbd1zmC64UJpH6vhnmajD35fsHLYgwDEe4l6qP3LAAABV0M2HDgAAAQDAEcwRQIgQt2xO8Ouu9dmieZzj4HoCqGfkrBg1hVHdLHqcXI2juYCIQDy4Mgu9tfz+igNKvceakpzRaiHpcHxACfGaXAjPAWY8wB3AN3rHSt6DU+mIIuBrYFocH4ujp0B1VyIjT0RxM227L7MAAABV0M2IKUAAAQDAEgwRgIhAJeRXMEmO5vIljaCTg41VwN2ohtNLOMiBh+Miw6aX0KmAiEA+mDiBJbXhYf+83EQeHM/pPREzNpvpjxr0FKHKwekAWUAdQBWFAaaL9fC7NP14b1Esj7HRna5vJkRXMDvlJhV1onQ3QAAAVdDNiRsAAAEAwBGMEQCIAWRGcBbp3DBKnllOQ7XFayEsQyY7V1UK07x3kee4ujNAiBmGaH1B/z7RhwYtc5h+d8PCZ98ye0/p21rbwdd85Sp4TANBgkqhkiG9w0BAQsFAAOCAQEAgPwS/KG+sunciHBP1PFDhDBc/qvo0Rj3DNsxFzHG9B0mUD9bGB2MTcLDePocS9ya4nJ1zHyi/tPoecsyt7bQnvapzZ3G2uha2acGt8yoKrAyH/ROKUL0aMHqIP9uA9qmIeURNV2Sq7+1ej+j5St/79ZYntd4eoXUMgKzECxEZZ/HeBworN7JtUxcM1ERyGVLczEjQkGCfqX1wXhO5Do167TdmskqJGhlt6NsUgz4wQmMz4uIOXeuj+RNWjFqyd1Ak73fOVUaGGNlBzCxXyAOa82Q4EOQxMT6qSAndUjEhruNt2Gl4V5mPQOMy/UmbckMq/PRxSxjqszPSZ+mRT2xNg==</dsig:X509Certificate>
      </dsig:X509Data>
      <dsig:KeyValue>
        <dsig:RSAKeyValue>
          <dsig:Modulus>4yAjw3LpTLqsTLCMa96yW5vtdWQYr9csrqMhMO8qInYMAEuvVA5sEhl/1uiQQYWQGERu9I4fsqafyof1khdF9A8A52rL6IImRq/X+EVjnMXdiIiG4a6gXBhcIdNiCuTt0ZCteeIhccHvE1X0JFQYPItULJMEysVd9mpJL/d0VQoDJDwnM5p2lghb4rdrmf4CQ8ROgkVxXjywimd8uKW1sayjzaOYkQ4getdV+CzNGLamsqHZQTzS4cg8WNf0Oza/GmHelODs0RKv/pMzFrNw+0EbmuNI2cL1YFr+6IfyOnyUmUwpqjyOWJSmEdpKl1WROnw9JnyF6JQ8M/Mp6w7TAw==</dsig:Modulus>
          <dsig:Exponent>AQAB</dsig:Exponent>
        </dsig:RSAKeyValue>
      </dsig:KeyValue>
    </dsig:KeyInfo>
  </dsig:Signature>
  <saml:Subject>
    <saml:NameID Format="urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified">Julien Fastr&#xE9;</saml:NameID>
    <saml:SubjectConfirmation Method="urn:oasis:names:tc:SAML:2.0:cm:bearer">
      <saml:SubjectConfirmationData NotOnOrAfter="2017-05-12T08:36:26.500Z"/>
    </saml:SubjectConfirmation>
  </saml:Subject>
  <saml:Conditions NotBefore="2017-05-12T08:36:24.500Z" NotOnOrAfter="2017-05-12T10:16:24.500Z">
    <saml:AudienceRestriction>
      <saml:Audience>https://abrumet.be</saml:Audience>
    </saml:AudienceRestriction>
  </saml:Conditions>
  <saml:AuthnStatement AuthnInstant="2017-05-12T08:36:26.501Z" SessionIndex="ID_6f7f163b-c24b-4db2-8e27-5d33df49daae">
    <saml:AuthnContext>
      <saml:AuthnContextClassRef>urn:oasis:names:tc:SAML:2.0:ac:classes:unspecified</saml:AuthnContextClassRef>
    </saml:AuthnContext>
  </saml:AuthnStatement>
  <saml:AttributeStatement>
    <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:subject-id">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">Julien Fastr&#xE9;</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute Name="urn:oasis:names:tc:xspa:1.0:subject:npi">
      <saml:AttributeValue>
        <Role xmlns="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="123456789" codeSystem="1.3.6.1.4.1.21297.100.1.1" codeSystemName="Rijksregisternummer" displayName="Julien Fastr&#xE9;" xsi:type="CE"/>
      </saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute FriendlyName="Patient Privacy Policy Acknowledgement Document" Name="urn:ihe:iti:bppc:2007:docid" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:anyURI">urn:oid:1.3.6.1.4.1.48336.1.2.11.2.80061626923</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute Name="urn:oasis:names:tc:xacml:2.0:resource:resource-id">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">80061626923^^^&amp;1.3.6.1.4.1.21297.100.1.1&amp;ISO</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute FriendlyName="Patient Relationship" Name="urn:enovation:xua:2016:patientRelationship" NameFormat="urn:oasis:names:tc:SAML:2.0:attrname-format:uri">
      <saml:AttributeValue xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="xs:string">true</saml:AttributeValue>
    </saml:Attribute>
    <saml:Attribute Name="urn:oasis:names:tc:xacml:2.0:subject:role">
      <saml:AttributeValue>
        <Role xmlns="urn:hl7-org:v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" code="112247003" codeSystem="2.16.840.1.113883.6.96" codeSystemName="SNOMED_CT" displayName="doctor" xsi:type="CE"/>
      </saml:AttributeValue>
    </saml:Attribute>
  </saml:AttributeStatement>
</saml:Assertion>        
XML;
}
